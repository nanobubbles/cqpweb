'''#______ create list of txt files from img ______#

#### Nanobubbles Project : CQPWeb
#### 2023
#### Sanda Hachana

# For scanned files before converting them into TEI'''

import os
import ntpath



def create_list (folder):
    """_summary_
    this function write the content of an img to a TXT file

    Returns:
        folder with a list of TXT files
    """
    list = []

    initial_count = 0
    dir = folder
    for path in os.listdir(dir):
        if os.path.isfile(os.path.join(dir, path)):
            initial_count += 1
            list.append(ntpath.basename(folder)+"/"+ntpath.basename(path)) 
    
    print(initial_count)
    
    with open(folder+"_list.txt", 'w') as l:
        for item in list:
            l.write("%s\n" % item)


if __name__ == "__main__":
    folder="FOLDER"
    create_list(folder)
