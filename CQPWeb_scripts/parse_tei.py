'''#______ parse TEI files by using BeautifulSoup______#

#### Nanobubbles Project : CQPWeb
#### Since 2022
#### Ali Raisi & Sanda Hachana

###needed scripts : common.py, bscommon.py
###script called by tei2vrt.py

# Exctrat several part of TEI files'''


from cmath import nan
#this library give access to mathematical functions for complex numbers
from tkinter.ttk import Separator
from xml.sax.handler import property_declaration_handler
# library use for web scraping purposes to pull the data out of html and xml files
# it creates a parse tree from page source code that can be used to extract data in hierarchical and more readable manner.
from bs4 import BeautifulSoup as bs
from soupsieve import select
from nltk import tokenize

import common
import bscommon
import os



def extract_abstract(soup):
    """_summary_
    this function extract the abstract of the TEI by using bs4
    Args:
        soup : beautiful soup
    Returns:
        _type_: return abstract
    """

    abs_str = bscommon.get_element(soup, 'abstract')
    return abs_str



def extract_file_contents(soup):
    """_summary_
    this function extract the content from the TEI by using bs4
    Args:
        soup : beautiful soup
    Returns:
        _type_: return content
    """
     
    search_list = ["text", "body", "div"]
    section_tei_list = bscommon.xml_get_children(soup, search_list)

    file_str = str()
    for cur_section_tei in section_tei_list:
        cur_section_str = str()
        cur_para_tei_list = bscommon.xml_get_children(cur_section_tei, ["p"])
        for cur_para_tei in cur_para_tei_list:
            cur_para_str = str()
            inner_para = bscommon.get_inner_xml(cur_para_tei)
            lines = inner_para.split(". ")
            for line in lines:
                #if not '<ref' or not 'bibr' in line:
                    line_soup = bs(line, "lxml")
                    line_str = line_soup.text.strip()

                    if line_str != "":
                        if line_str[-1:] != ".":
                            cur_para_str = cur_para_str + line_str + ". "

            if cur_para_str != "":
                #cur_section_str = cur_section_str + cur_para_str + "\n"
                cur_section_str = cur_section_str + cur_para_str

        if cur_section_str != "":
            #file_str = file_str + cur_section_str + "\n"
            file_str = file_str + cur_section_str

    return file_str


def extract_author_affiliation(soup):
    """_summary_
    this function extract the authors affiliations from the TEI by using bs4
    Args:
        soup : beautiful soup
    Returns:
        _type_: return authors affiliations
    """

    author_affiliation = ''

    try:
        file_soup = soup.select('teiHeader')[0].select('fileDesc')[0].select('sourceDesc')[0].select('biblStruct')[0].select('analytic')[0]
        author_soup_lst = file_soup.find_all('affiliation')
       
    except Exception:
        pass
    
    for i in author_soup_lst:
        author_affiliation = author_affiliation + ((i.get_text().strip()).replace("\n", " ")) + ","

    return author_affiliation



def extract_authors(soup):
    """_summary_
    this function extract the authors from the TEI by using bs4
    Args:
        soup : beautiful soup
    Returns:
        _type_: return authors 
    """

    authors = ''

    try:

        file_soup = soup.select('teiHeader')[0].select('fileDesc')[0].select('sourceDesc')[0].select('biblStruct')[0].select('analytic')[0]
        author_soup_lst = file_soup.find_all('persName')
    
    except Exception:
        pass

    for author_soup in author_soup_lst:

        surname = author_soup.surname.get_text().strip()
        if "forename" in author_soup:
            firstname = author_soup.forename.get_text().strip()
            authors = authors + '{} {}'.format(surname,firstname) + ","
        else:
            authors = authors + '{}'.format(surname) + ","
            print("no forename")

    return authors



def extract_title(soup):
    """_summary_
    this function extract the title of the TEI by using bs4
    Args:
        soup : beautiful soup
    Returns:
        _type_: return title
    """

    title = nan
    try:
        title_soup = soup.select('teiHeader')[0].select('fileDesc')[0].select('titleStmt')[0]
        title = bscommon.get_element(title_soup,'title')
        
    except Exception:
        pass

    return title



def extract_publisher(soup):
    """_summary_
    this function extract the publishers from the TEI by using bs4
    Args:
        soup : beautiful soup
    Returns:
        _type_: return publishers
    """

    publisher = nan
    
    try:
        publisher_soup = soup.select('teiHeader')[0].select('fileDesc')[0].select('publicationStmt')[0]
        publisher = bscommon.get_element(publisher_soup, 'publisher')
    
    except Exception:
        pass
    
    return publisher



def extract_pubyr(soup):
    """_summary_
    this function extract the publication year from the TEI by using bs4
    Args:
        soup : beautiful soup
    Returns:
        _type_: return authors affiliations
    """
    
    pub_yr = nan
    
    try:
        publisher_soup = soup.select('teiHeader')[0].select('fileDesc')[0].select('publicationStmt')[0]
        pub_yr = bscommon.get_element(publisher_soup, 'date')
    
    except Exception:
        pass
    
    return pub_yr



def get_ip(tei_folder):
    """_summary_
    this function create a list with all the info by calling the previous functions one by one and adding them to new variables
    Args:
        tei_folder : folder
    Returns:
        _type_: list with all the information extracted previously
    """
    
    print("++++++++")
    print("tei_folder: '"+str(tei_folder)+"'")
    print("++++++++")
    
    tei_fil_nam_lst = common.get_file_list(tei_folder, "tei")
    ip_lst = []
    
    for tei_fil_nam in tei_fil_nam_lst:
        new_path = os.path.join(tei_folder,tei_fil_nam)
        print("tei_file_nam : '"+tei_fil_nam+"'")

        with open(new_path, 'r', encoding='utf8') as tei:
            soup = bs(tei, "xml")

            txt_id = common.get_unique_txt_id()
            abstract = extract_abstract(soup)
            file_cont = extract_file_contents(soup)
            # citation = extract_citation(soup)
            title = extract_title(soup)
            authors = extract_authors(soup)
            author_affiliation = extract_author_affiliation(soup)
            publisher = extract_publisher(soup)
            pub_yr = extract_pubyr(soup)
            
            ip_lst.append((txt_id, abstract, file_cont, title, authors, author_affiliation, publisher, pub_yr))

    return ip_lst
