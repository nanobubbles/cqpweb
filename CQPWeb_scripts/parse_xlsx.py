'''#______ parse XLSX files by using BeautifulSoup ______#

#### Nanobubbles Project : CQPWeb
#### 2022
#### Ali Raisi

###script called by tei2vrt.py

# Exctrat several part of XLSX files '''

from cmath import nan
import pandas as pd
import numpy as np
import glob, os
from bs4 import BeautifulSoup
import spacy
import random
import string
import glob
import os



def gen_rand_txt_id():
    """_summary_
    this function is responsible for generating random text id.

    Returns:
        _type_: return a text id combined string lowercasem uppercase + digits
    """
    chars = string.ascii_lowercase + string.ascii_uppercase + string.digits
    #variable chars store the order of generated random txt id Returns  _type_: _description_
    
    rand_txt_id = str()
    #it is an empty string to store random txt id

    for i in range(3):
        """_summary_
        range() produce a sequence of numbers starting from 0 bydefault and increment by 1

        the for-loop in range of sequence start from zero until 3 (excluding 3)
        """
        rand_txt_id_ch = random.choice(chars)
        #randomly choose the three generated id
 
        rand_txt_id = rand_txt_id + rand_txt_id_ch
        #combining the randomly generated txt it with empty string => store in variable rand_txt_id and return it

    return rand_txt_id



def get_unique_txt_id():
    """_summary_
    this function is to make sure the generated random txt_id must be unique for each document

    Returns:
        _type_: _description_
    """
    global txt_id_lst
   
    txt_id = gen_rand_txt_id()
    #pass the function gen_rand_txt_id 
    
    while txt_id in txt_id_lst:
        """_summary_
        if the new txt_id is not in the list of before generated txt_id_lst
        call the function gen_rand_txt_id and store it in txt_id and append it to the list
        then return that unique txt_id
        """
        txt_id = gen_rand_txt_id()
    
    txt_id_lst.append(txt_id)

    return txt_id



def get_txt_id(pub_id):
    """_summary_
    this function reform the txt_id with special format and return the txt_id
    Args:
        pub_id (_type_): _description_

    Returns:
        _type_: _description_
    """
    txt_id = pub_id.replace(pub_id[3], 'xxpointxx')
    
    return txt_id



def get_ip_xlsx(xslx_fil_nam):
    """_summary_
    this function reform the txt_id with special format and return the txt_id
    Args:
        xslx_fil_nam (_type_): excel file

    Returns:
        _type_: list with all the elements
    """
    df = pd.read_excel(xslx_fil_nam, dtype=str)
    
    pub_id_lst = df['Publication ID'].tolist()
    txt_id_lst = [get_unique_txt_id() for pub_id in pub_id_lst]

    abstract_lst = df['Abstract'].tolist()

    author_affiliation_lst = df['Authors Affiliations'].tolist()
    
    publisher_lst = df['Publisher'].tolist()
    
    pub_yr_lst = df['PubYear'].tolist()
    
    ip_lst = list(zip(abstract_lst, txt_id_lst, author_affiliation_lst, publisher_lst, pub_yr_lst))

    return ip_lst



def get_tei_element(soup, node_name):
    """_summary_
    this function gets an element from the tei file
    Args:
        soup : bs4
        node_name : tei content

    Returns:
        _type_: content
    """
    ele = nan
    
    try:
        ele = getattr(soup, node_name).getText().strip()
    
    except Exception:
        pass
    
    return ele



def get_ip_tei(tei_folder):
    """_summary_
    this function creates a list with all the elements extracted from previous functions
    Args:
        tei_folder (_type_): folder

    Returns:
        _type_: list with all the elements
    """

    os.chdir(tei_folder)
    
    tei_fil_nam_lst = list(glob.glob('*.tei'))
    
    if len(tei_fil_nam_lst) == 0:
        raise Exception('No tei files present')
    
    ip_lst = []
    
    for tei_fil_nam in tei_fil_nam_lst:
        
        with open(tei_fil_nam, 'r') as tei:
            soup = BeautifulSoup(tei, 'lxml')
            abstract = get_tei_element(soup, 'abstract')
            txt_id = nan
            author_affiliation = nan
            
            try:
                file_soup = soup.select('teiHeader')[0].select('fileDesc')[0].select('sourceDesc')[0].select('biblstruct')[0]
                pub_id = get_tei_element(file_soup, 'idno')
                txt_id = get_unique_txt_id()
                author_soup_lst = file_soup.select('author')
                
                for idx, author_soup in enumerate(author_soup_lst):
                    surname = author_soup.surname.getText().strip()
                    forename = author_soup.forename.getText().strip()
                    orgname = author_soup.orgname.getText().strip()
                    cur_author_affiliation = '{} {}({})'.format(surname, forename, orgname)
                    
                    if idx == 0:
                        author_affiliation = cur_author_affiliation
                    
                    else:
                        author_affiliation = author_affiliation + ', ' + cur_author_affiliation
            
            except Exception:
                pass

            publisher = nan
            pub_yr = nan
            
            try:
                publisher_soup = soup.select('teiHeader')[0].select('fileDesc')[0].select('publicationStmt')[0]
                publisher = get_tei_element(publisher_soup, 'publisher')
                pub_yr = get_tei_element(publisher_soup, 'date')
            
            except Exception:
                pass

            ip_lst.append((abstract, txt_id, author_affiliation, publisher, pub_yr))

    return ip_lst

def calc_op(ip_lst):
    """_summary_
    this function creates the content of the VRT and META files and create the files
    Args:
        ip_list (_type_): list

    Returns:
        _type_: files (2)
    """
    sp = spacy.load('en_core_web_sm')
    vrt_op = ''
    meta_op = ''
    
    for (abstract, txt_id, author_affiliation, publisher, pub_yr) in ip_lst:
        html_sentences = str()
        sent_list = []
        abs = sp(abstract)
        
        for sent in abs.sents:
            sent_list.append(sent.text)
            word_lst = sp(sent.text)
            pos_sentence = str()
            
            for word in word_lst:
                pos_word = '{}\t{}\t{}\t{}\n'.format(word.text, word.pos_, word.lemma_, word.ent_type_)
                pos_sentence = pos_sentence + pos_word
            
            html_sentence = '<s>\n{}</s>\n'.format(pos_sentence)
            html_sentences = html_sentences + html_sentence
        
        html_output = '<text id = "{}">\n{}</text>\n'.format(txt_id, html_sentences)
        vrt_op = vrt_op + html_output
        meta_sentence = '{}\t{}\t{}\t{}\n'.format(txt_id, author_affiliation, publisher, pub_yr)
        meta_op = meta_op + meta_sentence
    
    return (vrt_op, meta_op)



def get_op_fil_nam_fil(ip_path):
    """_summary_
    this function adds the correct extension to the VRT and META files
    Args:
        ip_path (_type_): path

    Returns:
        _type_: file (2)
    """
    vrt_op_fil_nam = os.path.splitext(ip_path)[0] + '.vrt'
    meta_op_fil_nam = os.path.splitext(ip_path)[0] + '.txt'
    
    return (vrt_op_fil_nam, meta_op_fil_nam)



def get_op_fil_nam_dir(ip_path):
    """_summary_
    this function adds the correct directory to the VRT and META files
    Args:
        ip_path (_type_): path

    Returns:
        _type_: file (2)
    """
    ip_dir_nam = os.path.basename(ip_path)
    vrt_op_fil_nam = os.path.join(ip_path, ip_dir_nam + '.vrt')
    meta_op_fil_nam = os.path.join(ip_path, ip_dir_nam + '.txt')
    return (vrt_op_fil_nam, meta_op_fil_nam)



def gen_op(ip_path):
    """_summary_
    this function creates the VRT and META files by using the previous functions
    Args:
        ip_path (_type_): path

    Returns:
        _type_: content
    """
    
    if os.path.isdir(ip_path):
        (vrt_op_fil_nam, meta_op_fil_nam) = get_op_fil_nam_dir(ip_path)
        ip_lst = get_ip_tei(ip_path)
    
    elif os.path.isfile(ip_path):
        (vrt_op_fil_nam, meta_op_fil_nam) = get_op_fil_nam_fil(ip_path)
    
        if ip_path.lower().endswith('.xlsx'):
            ip_lst = get_ip_xlsx(ip_path)
    
        else:
            raise Exception('Do not recognize the file format')
    
    else:
        raise Exception('Invalid path')

    (vrt_op, meta_op) = calc_op(ip_lst)

    with open(vrt_op_fil_nam, 'w') as vrt_fil:
        vrt_fil.write(vrt_op)

    with open(meta_op_fil_nam, 'w') as meta_fil:
        meta_fil.write(meta_op)


tei_folder = 'FOLDER'

txt_id_lst = list()
# unique text id generator

for ip_path in [tei_folder]:
    gen_op(ip_path)
    print(ip_path)

