import os
import ntpath
from os import listdir
from os.path import isfile, join

#convert pdf to image 
import pdf2image
from pdf2image import convert_from_path

#convert imgS to pdfS
import ntpath
from PIL import Image
import pytesseract

#create 1 pdf from pdfS
from pypdf import PdfMerger
#from PyPDF2 import PdfWriter

#sort list in the good order
from natsort import natsorted

#count nb of pages of PDF
import PyPDF2

os.mkdir('results')

#main function to execute the script
def convert(file):
    # fichiers = [f for f in listdir("files") if isfile(join("files", f))]
    # print(fichiers)


    # for i in fichiers:
        # #convert pdf to imgS 
        # file = i

        print('***************working on file '+file+' ***************')
        
        #open file
        opfile = open ('C:/Users/hachanas/Documents/CQPWEB/script/patents/files/'+file, 'rb')

        #count PDF's nb of pages
        pdfReader = PyPDF2.PdfReader(opfile)
        nbpage = len(pdfReader.pages)
        print(f"Total Pages: {nbpage}")

        #convert scanned PDF to imgS
        print('*****convert pages to imgS*****')
        pages = []

        if nbpage > 10:
            nbchunk = nbpage // 10
            print("nbchunck= "+str(nbchunk))
            n = 0
            # pages = convert_from_path('files/'+file, last_page=0)
            # print("*********PAGES = 0**********")
            # print (len(pages))
            # print(pages)
            for n in range(nbchunk):
                firstp=n*10+1
                lastp=n*10+10
                print(firstp, lastp)
                p = convert_from_path('files/'+file, first_page=firstp, last_page=lastp)
                print("*********P**********")
                print (len(p))
                print(p)
                pages = pages + p
                print("*********PAGES**********")
                print (len(pages))
                print (pages)
                print(n)
            p2 = convert_from_path('files/'+file, first_page=nbchunk*10+1, last_page=nbpage)
            print("*********P2**********")
            print (len(p2))
            print (p2)
            pages = pages + p2
        else:
            pages = convert_from_path('files/'+file)

        print(len(pages))
        name = file[:-4]

        for i in range(len(pages)):
            pages[i].save('img/'+name+str(i) +'.jpg', 'JPEG')
            print('page '+ str(i))


        #convert imgS to pdfS
        folder="img"
        create_list(folder)



        #create 1 pdf from pdfS
        pdfs = "pdfs"
        list = []
        dir = pdfs

        merger = PdfMerger()
        print('*****merges pdfS to one pdf*****')
        #merger = PdfWriter()

        for path in os.listdir(dir):
            if os.path.isfile(os.path.join(dir, path)):

                list.append(ntpath.basename(pdfs)+"/"+ntpath.basename(path))
                #print(path)
            print('list')
            print(list)

        llist = natsorted(list)
        print('llist')
        print(llist)
        for pdf in llist:
            merger.append(pdf)

        with open ("results/"+name+".pdf", 'wb') as r:
            merger.write(r)
            merger.close()
        r.close()
        
        delete_files('img')
        delete_files('pdfs')

        

#convert imgS to pdfS
def create_list (folder):
    print('*****convert imgS to pdfS*****')
    list = []

    initial_count = 0
    dir = folder
    for path in os.listdir(dir):
        if os.path.isfile(os.path.join(dir, path)):
            initial_count += 1
            list.append(ntpath.basename(folder)+"/"+ntpath.basename(path))
            print(path)

            ima = "img/"+path
            print(ima)

            pdf = pytesseract.image_to_pdf_or_hocr(ima, extension='pdf')

            with open('pdfs/'+path+'.pdf', 'w+b') as f:
                f.write(pdf)
            f.close()
    
    print("total pages : "+ str(initial_count))
    #print(list)


    #####inutile??? 
    with open("_list.txt", 'w') as l:
        for item in list:
            l.write("%s\n" % item)
    l.close()
    ##############
            

def delete_files (folder):
    folder = folder
    for filename in os.listdir(folder): 
        file_path = os.path.join(folder, filename)  
        try:
            if os.path.isfile(file_path):
                os.remove(file_path)  
            elif os.path.isdir(file_path):  
                os.rmdir(file_path)  
        except Exception as e:  
            print(f"Error deleting {file_path}: {e}")
    print("Deletion done")





# if __name__ == "__main__":

#     folder = 'ici'
#     ip_path = folder

#     for ip_path in [folder]:
#         convert(ip_path)


