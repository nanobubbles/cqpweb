#______ convert PDF files to TEI files ______#

#### Nanobubbles Project
#### March 2023
#### Sanda Hachana

# Convert PDF files with the GROBID request (see https://nanobubbles.univ-grenoble-alpes.fr/grobid/)

#############TEST FOR PDF SCANNED --> EXECUTE NEW.PY IF NOT REAL PDF
import os
from os import listdir
from os.path import isfile, join
import requests

import new2

#__create TEI folder
os.mkdir('tei')


def grobid(path, tei_path, i):
    #__GROBID request
    file = {'input': open(path+i, 'rb')}
    response = requests.post('https://nanobubbles.univ-grenoble-alpes.fr/grobid/api/processFulltextDocument', files=file)
    #print(response.text)

    #__create TEI file
    name_tei = i[:-4]+'.tei'
    with open(tei_path+name_tei, 'wb') as t:
        t.write(response.content)
    t.close()


#__add all the PDF files to a list
fichiers = [f for f in listdir("files") if isfile(join("files", f))]
print(fichiers)
path_files = "C:/Users/hachanas/Documents/CQPWEB/script/patents/files/"
tei = "C:/Users/hachanas/Documents/CQPWEB/script/patents/tei/"

for i in fichiers:
    a = grobid(path_files, tei, i)


source = os.listdir("C:/Users/hachanas/Documents/CQPWEB/script/patents/tei")
print (source)
for fg in source:
    print("********"+fg)
    filepath = "C:/Users/hachanas/Documents/CQPWEB/script/patents/tei/"+fg
    e = os.path.getsize(filepath)
    if (e < 1000):
        print (fg+" = lancer script new.py")
        file2convert = new2.convert(fg[:-3]+'pdf')
        print(file2convert)
    else:
        print (fg+" = ok")

os.mkdir('teiscanned')

sourceres = os.listdir("C:/Users/hachanas/Documents/CQPWEB/script/patents/results")
print (sourceres)
for fr in sourceres:
        path = "C:/Users/hachanas/Documents/CQPWEB/script/patents/results/"
        tei_path = "C:/Users/hachanas/Documents/CQPWEB/script/patents/teiscanned/"
        fileok = grobid(path, tei_path, fr)

   

