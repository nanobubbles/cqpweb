#______ convert PDF files to TEI files ______#



#### Nanobubbles Project

#### March 2023

#### Sanda Hachana



# Convert PDF files with the GROBID request (see https://nanobubbles.univ-grenoble-alpes.fr/grobid/)



#############TEST FOR PDF SCANNED --> EXECUTE NEW.PY IF NOT REAL PDF

import os

from os import listdir

from os.path import isfile, join

import requests



import new2c







#__ Grobid

def grobid(path, tei_path, i):

    #__GROBID request

    file = {'input': open(path+i, 'rb')}

    print("__GROBID request: "+str(i))

    name_tei = i[:-4]+'.tei'

    # if tei is too small or not exists: try again grobid

    # if size < 1ko and if file does not exists

    e = os.path.getsize(path)

    print(str(isfile(tei_path+name_tei)))

    if not isfile(tei_path+name_tei) or ( isfile(tei_path+name_tei) and (os.path.getsize(tei_path+name_tei) < 49)):

        response = requests.post('https://nanobubbles.univ-grenoble-alpes.fr/grobid/api/processFulltextDocument', files=file)

        #__create TEI file

        with open(tei_path+name_tei, 'wb') as t:

            t.write(response.content)

        t.close()

        print(tei_path+name_tei+" Created calling grobid")

    else:

        print(tei_path+name_tei+" Already exists, skipping grobid")

    #print(response.text)



    



##########

#__create TEI folder

try:

    os.mkdir('tei')

except FileExistsError:

  print("'tei' dir already exists")



##########

#__add all the PDF files to a list

fichiers = [f for f in listdir("files") if isfile(join("files", f))]

print(fichiers)

path_files = "files/"

tei = "tei/"



##########

#__ Use Grobid

for i in fichiers:

    # test if the file exists, to be done only if the corresponding tei does not exists

    a = grobid(path_files, tei, i)





##########

#__ Check tei, if tei not fine pdf has to be converted

source = os.listdir("tei")

print (source)

for fg in source:

    print("********************************")

    print("********"+fg)

    print("********************************")

    filepath = "tei/"+fg

    e = os.path.getsize(filepath)

    # if tei is too small: create a new pdf file that will be grobid readable: results/*.pdf

    # if this file exists the work has already be done (in a previous exec of the script)

    # if size < 1ko and if file does not exists

    if (e < 1000) and not isfile("results/"+fg[:-3]+'pdf') :

        print (fg+" = lancer script new.py")

        file2convert = new2c.convert(fg[:-3]+'pdf')

        print(file2convert)

    else:

        if (e >= 1000):

            print (fg+" = tei creation ok with grobid")

        else:

            print ("results/"+fg[:-3]+'pdf'+" already created probably a previous exec of the script")

            

try:

    os.mkdir('teiscanned')

except FileExistsError:

  print("'teiscanned' already exists")





sourceres = os.listdir("results")

print(sourceres)

for fr in sourceres:

        print(fr)

        path = "results/"

        tei_path = "teiscanned/"

        fileok = grobid(path, tei_path, fr)

print("All Done")

   



