'''#______ convert PDF files to TEI files ______#

#### Nanobubbles Project : CQPWeb
#### Since 2023
#### Sanda Hachana

# Convert PDF files with the GROBID request (see https://nanobubbles.univ-grenoble-alpes.fr/grobid/)'''

import os
from os import listdir
from os.path import isfile, join
import requests



#__create TEI folder
os.mkdir('FOLDER')

#__add all the PDF files to a list
fichiers = [f for f in listdir("PATH") if isfile(join("PATH", f))]
print(fichiers)


for i in fichiers:

    #__GROBID request
    file = {'input': open('PATH/'+i, 'rb')}
    response = requests.post('https://nanobubbles.univ-grenoble-alpes.fr/grobid/api/processFulltextDocument', files=file)
    #print(response.text)

    #__create TEI file
    name_tei = i[:-4]+'.tei'
    with open("PATH/"+name_tei, 'wb') as t:
        t.write(response.content)

