'''#______ create VRT and META file from TEI ______#

#### Nanobubbles Project : CQPWeb
#### Since 2022
#### Ali Raisi & Sanda Hachana

###needed scripts : common.py, parse_tei.py, (parse_xlsx.py & parse_xml.py)

##some modifications are still needed'''

import os
import spacy

import common, parse_tei as tei
#, parse_xlsx as xlsx
#, parse_xml as xml


#####create VRT 
##variables: (abstract), text, id
def get_abs_op(sp, abstract ,cont, txt_id):
    '''_summary_
    this function extracts abstract and content in single file
    Args:
        sp (type): spacy model
        abstract (type): _description_
        cont (type): _description_
        txt_id (type): _description_
    Returns:
        (type): _description_
    '''
    vrt_sentences = str()
    combine_abs_cont = ''
    combine_abs_cont = abstract + cont + "\n"
    sp_abs = sp(combine_abs_cont)

    for sent in sp_abs.sents:
        word_lst = sp(sent.text)
        pos_sentence = str()
        
        for word in word_lst:
            ######without TAG
            #pos_word = '{}\t{}\t{}\t{}\n'.format(word.text, word.pos_, word.lemma_, word.ent_type_)
            #pos_sentence = pos_sentence + pos_word
            ######with TAG
            tag_word = '{}\t{}\t{}\t{}\t{}\n'.format(word.text, word.pos_, word.tag_, word.lemma_, word.ent_type_)
            pos_sentence = pos_sentence + tag_word

        vrt_sentence = '<s>\n{}</s>\n'.format(pos_sentence)
        vrt_sentences = vrt_sentences + vrt_sentence

    vrt_output = '<text id = "{}">\n{}</text>\n'.format(txt_id, vrt_sentences)
    print(">>> Just done with txt id: "+txt_id)
    
    return vrt_output




def get_meta_op(txt_id, title, authors, author_affiliation, publisher, pub_yr, abstract):
    '''_summary_
    this function creates META
    Args:
        (type): _description_
    Returns:
        (type): _description_
    '''
    meta_sentence = '{}\t{}\t{}\t{}\t{}\t{}\t{}\n'.format(txt_id, title, authors, author_affiliation, publisher, pub_yr, abstract)
    
    return meta_sentence

####still need to work on this function
# def get_file_name(file_name):
    '''_summary_
    this function gets the file's name in the metadat
    Args:
        (type): _description_
    Returns:
        (type): _description_
    '''
#     file_name = file_name.replace('.txt', '.pdf')



def calc_op(ip_lst):
    '''_summary_
    this function creates the metadata list
    Args:
        (type): _description_
    Returns:
        (type): _description_
    '''
    print("==================== Loading spacy model ==========")
    sp = spacy.load('en_ner_bc5cdr_md')
    abs_op = ''
    meta_op = ''
    i=0
    for (txt_id, abstract, cont, title, authors, author_affiliation, publisher, pub_yr) in ip_lst:
        print(">>> Now working on txt ("+str(i)+") id: "+str(txt_id))
        abs_op = abs_op + get_abs_op(sp, abstract, cont, txt_id)

        ####when function get_file_name will be done
        #file_name = get_file_name(ip_lst)
        #meta_op = meta_op + get_meta_op(txt_id, file_name, title, authors,author_affiliation, publisher, pub_yr, abstract)
        
        meta_op = meta_op + get_meta_op(txt_id, title, authors,author_affiliation, publisher, pub_yr, abstract)

    return (abs_op, meta_op)



def gen_op(ip_path):
    '''_summary_
    this function combines content to abstract to write both in tei.abs
    Args:
        type : path
    '''
    print("*******")
    print("ip_path: '"+str(ip_path)+"' ")
    print("*******")

    if os.path.isdir(ip_path):
        (abs_op_fil_nam,meta_op_fil_nam) = common.get_op_fil_nam_dir(ip_path)
        ext = common.get_file_ext(ip_path)
        if ext == "tei":
            ip_lst = tei.get_ip(ip_path)
        elif ext == "xml":
            ip_lst = xml.get_ip(ip_path)
        else:
            raise Exception('Do not recognize the file format')
    
    elif os.path.isfile(ip_path):
        (abs_op_fil_nam, meta_op_fil_nam) = common.get_op_fil_nam_fil(ip_path)
        if ip_path.lower().endswith('.xlsx'):
            ip_lst = xlsx.get_ip(ip_path)
        else:
            raise Exception('Do not recognize the file format')
    
    else:
        raise Exception('Invalid path')

    print("*******")
    print("*** Pos / ent ****")

    (abs_op, meta_op) = calc_op(ip_lst)

    print("*** Pos / ent : done ****")

    with open(abs_op_fil_nam, 'w', encoding='utf8') as abs_fil:
        print("*******")
        abs_fil.write(abs_op)

    with open(meta_op_fil_nam, 'w', encoding='utf8') as meta_fil:
        meta_fil.write(meta_op)



def success_ex():
    '''_summary_
    the end 
    '''
    return "end of excution"



if __name__ == "__main__":

    tei_folder = 'FOLDER'
    ip_path = tei_folder

    for ip_path in [tei_folder]:
        gen_op(ip_path)

    
