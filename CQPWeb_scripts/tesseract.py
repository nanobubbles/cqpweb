'''#______ create PDF with tesseract ______#

#### Nanobubbles Project
#### 2023
#### Sanda Hachana

'''


import os
import ntpath

from PIL import Image
import pytesseract



def create_list (folder):
    '''_summary_
    this function converts a list of img of a scanned PDF to a list of PDF files by using tesseract
    Args:
        type : _path_
    Returns:
        files
    '''
    list = []

    initial_count = 0
    dir = folder
    
    for path in os.listdir(dir):
        if os.path.isfile(os.path.join(dir, path)):
            initial_count += 1
            list.append(ntpath.basename(folder)+"/"+ntpath.basename(path))
            print(path)

            ima = "img/"+path
            print(ima)

            pdf = pytesseract.image_to_pdf_or_hocr(ima, extension='pdf')

            with open(path+'.pdf', 'w+b') as f:
                f.write(pdf)
    
    print(initial_count)

    with open(folder[0:5]+"_list.txt", 'w') as l:
        for item in list:
            l.write("%s\n" % item)
  


if __name__ == "__main__":
    folder="PATH"
    create_list(folder)



