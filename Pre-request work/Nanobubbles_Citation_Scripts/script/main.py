from cmath import nan
from distutils.filelist import findall
from itertools import count
from logging import exception
from multiprocessing.sharedctypes import Value
from operator import countOf
from tkinter.ttk import Separator
from weakref import ref
from xml.sax.handler import property_declaration_handler
# library use for web scraping purposes to pull the data out of html and xml files
# it creates a parse tree from page source code that can be used to extract data in hierarchical and more readable manner.
from bs4 import BeautifulSoup as bs
from soupsieve import select
import re

import common
import bscommon
from nltk import tokenize

# metadata of citing paper
# =================start============
def extract_authors(soup):

    authors = ''
    try:

        file_soup = soup.select('teiHeader')[0].select('fileDesc')[0].select('sourceDesc')[0].select('biblStruct')[0].select('analytic')[0]
        author_soup_lst = file_soup.find_all('persName')


        for author_soup in author_soup_lst:

            surname = author_soup.surname.get_text().strip()
            firstname = author_soup.forename.get_text().strip()
            authors = authors + '{} {}'.format(surname, firstname) +', '
        authors = authors.rstrip(', ')
    
    except Exception:
        pass
    return authors

def extract_publisher(soup):
    publisher = nan
    try:
        publisher_soup = soup.select('teiHeader')[0].select('fileDesc')[0].select('publicationStmt')[0]
        publisher = bscommon.get_element(publisher_soup, 'publisher')
    except Exception:
        pass
    return publisher

def extract_pubyr(soup):
    pub_yr = nan
    try:
        publisher_soup = soup.select('teiHeader')[0].select('fileDesc')[0].select('publicationStmt')[0]
        pub_yr = bscommon.get_element(publisher_soup, 'date')
    except Exception:
        pass
    return pub_yr

def extract_title(soup):
    title = nan
    try:
        title_soup = soup.select('teiHeader')[0].select('fileDesc')[0].select('titleStmt')[0]
        title = bscommon.get_element(title_soup,'title')
        # title = title.get_text().strip()
        title = title.replace("<scp>COVID</scp> ‐19", "COVID-19")

        
    except Exception:
        pass

    return title

def extract_doi(soup):
    doi = nan
    try:
        doi_soup = soup.select('teiHeader')[0].select('fileDesc')[0].select('sourceDesc')[0].select('biblStruct')[0]
        doi = doi_soup.find('idno', type='DOI')
        doi = doi.get_text().strip()
        # doi = bscommon.get_element(doi_soup,'idno', type="DOI")
        
        
    except Exception:
        pass

    return doi

# =================end==============

def find_sentece(para_tei):
    inner_xml = bscommon.get_inner_xml(para_tei)
    
    split_sentences = tokenize.sent_tokenize(inner_xml)
    return split_sentences

def find_cite_lines(para_tei):
    # inner_para = bscommon.get_inner_xml(para_tei)
    # lines = inner_para.split(". ")
    # lines = find_sentece(para_tei+)
    # lines = parenthesis_split(para_tei,separator=".", lparen="(",rparen=")")
    lines = find_sentece(para_tei)
    print("+++++++++++++ lines++++++++++++++++++++++")
    print(lines)
    # para_tei = re.sub(r', *((?:<ref type="bibr".*?</ref>)+)', r'\g<1>\.', para_tei)
    # lines = tokenize.sent_tokenize(para_tei)

    cite_lines = list()
    pattern = re.compile(r'(<ref .*type="bibr".*</ref>)')

    for i in range(0, len(lines) - 1):
        current_sentence = lines[i]
        next_sentence = lines[i + 1]
        match = re.match(pattern, next_sentence)
        if match:
            matched_part = match.group(0)
            lines[i] = current_sentence[:-1] + " " +matched_part + "."
            lines[i + 1] = next_sentence.replace(matched_part, "")
            cite_lines.append(lines[i])
        elif re.search(pattern, lines[i]):
            cite_lines.append(lines[i])

    if re.search(pattern, lines[-1]):
        cite_lines.append(lines[-1]) 
      
    print("+++++++++++++ cite_lines++++++++++++++++++++++")
    print(cite_lines)

    processed_cited_lines = []

    for i in range(0, len(cite_lines)):
        processed_cited_lines.append(re.sub(r', *((?:<ref.*?type="bibr".*?</ref>)+)', r' \g<1>,', cite_lines[i]))

    # for line in processed_cited_lines:
        # print("***************************", line)            
    return processed_cited_lines

# ===================main function=====================================

# def find_bib_by_ref(bib_ref_list, id):
#     bib_txt = ""
#     for bib_ref in bib_ref_list:
#         if id == bib_ref.get("xml:id"):
#             bib_txt = bib_ref.get_text(separator=' ')
#             # all metadata related to one ref in online instead of mutiple line.
#             bib_txt = bib_txt.replace("\n", "")
            
#     # bib_txt is reference metadat come in one line with space separator
#     # how to bring id-number of the reference infront of each bib_txt?
#     return bib_txt

# ================start of new bibref=================================

def find_bib_by_ref(bib_ref_list, id):
    bib_txt = ""
    authors_list = ""
    text_of_elements = ""
    ref_author = ""
    ref_authors = ""
    ref_authors_format = ""
    ref_title = ""
    ref_title_text_format = ""
    ref_pub_title = ""
    ref_published_year=""
    for bib_ref in bib_ref_list:
        if id == bib_ref.get("xml:id"):
            
            try:

                if bib_ref.analytic:                
                    authors_list = bib_ref.analytic.find_all('author')
                    # print("===author_list_analytic===", authors_list)
                    for i,aut in enumerate(authors_list):
                        ref_author = aut.get_text(separator=" ").strip()
                        ref_authors = ref_authors + ref_author +', '
                    #     bib_txt = "{}.".format(ref_authors)
                    #     bib_txt = bib_txt.replace('\n',' ')
                    ref_authors = ref_authors.rstrip(', ')

                    ref_title = bib_ref.analytic.title.get_text()
                elif bib_ref.monogr:
                    authors_list = bib_ref.monogr.find_all('author')
                    print("===author_list_monogr===", authors_list)
                    for i,aut in enumerate(authors_list):
                        ref_author = aut.get_text(separator=" ").strip()
                        ref_authors = ref_authors + ref_author +', '
                    #     bib_txt = "{}.".format(ref_authors)
                    #     bib_txt = bib_txt.replace('\n',' ')
                    ref_authors = ref_authors.rstrip(', ')
                
                
                ref_published_year = bib_ref.imprint.date
                ref_published_year = ref_published_year.attrs["when"]
                ref_pub_title = bib_ref.monogr.title.get_text()
                
            except Exception:
                pass
               
            
            bib_txt = ". {}. {}. {}. {}.".format(ref_authors, ref_title, ref_pub_title, ref_published_year )
                
            return bib_txt

# ================end of new bibref===================================
def extract_citation(soup):
    search_list = ["text", "body", "div"]
    cur_search_tei_list = bscommon.xml_get_children(soup, search_list)

    cite_lines = list()
    for cur_search_tei in cur_search_tei_list:
        cur_para_list = bscommon.xml_get_children(cur_search_tei, ["p"])
        for cur_para in cur_para_list:
            cur_cite_lines = find_cite_lines(cur_para)
            cite_lines.extend(cur_cite_lines)

    citations = ""
    search_list = ["listBibl", "biblStruct"]
    # # search_list = ["listBibl", "biblStruct", "analytic", "title", "author"]
    bib_ref_list = bscommon.xml_get_children(soup, search_list)
    # bib_ref_list = soup.select('listBibl')[0].select('biblStruct')[0].select('analytic')[0]
    

    for cite_line in cite_lines:
        # calculate each cite line from founded "cite_lines" list 
        cite_line_soup = bs(cite_line, "lxml")

        # it citation context including all the reference numbers
        # cur_line_txt = cite_line_soup.text +"test" 
        cur_line_txt = cite_line_soup.text
        # cross_ref_list:find all setences contains tag ref
        cross_ref_list = cite_line_soup.find_all("ref")
        # cur_bib_txt = ""
        cur_bib_txt = list()
        
        for cross_ref in cross_ref_list:
            id_str = cross_ref.get("target")
            # for printing ref_number infront of metadata
            ref_cont = cross_ref.text        
            cur_ref_text = ""
            if id_str != None:
                id_list = id_str.split(" ")
                for id in id_list:
                    id = id.replace("#", "")
                    # cur_id_text = find_bib_by_ref(bib_ref_list, id)
                    cur_id_text = find_bib_by_ref(bib_ref_list, id)
                    if cur_id_text !=None:
                        # cur_ref_text = cur_ref_text + "\n" + cur_id_text
                        # new line between the citation removed
                        # cur_ref_text = cur_ref_text + cur_id_text
                        # cur_ref_text = cur_ref_text + '===' + id + ref_cont + cur_id_text
                        cur_ref_text = cur_ref_text + ref_cont + cur_id_text
                        # if i remove the \n then there wont be space between citations
                        # cur_ref_text == cur_bib_txt: contains metadata of references
                        # "cur_id_text" = metadatas for each reference number 
                        
            if cur_ref_text != "":
                # "cur_bib_txt" is a string that store all cur_ref_text
                # cur_bib_txt = cur_bib_txt +  "\n" + cur_ref_text
                cur_bib_txt.append(cur_ref_text)
                
                
                
        # print('current bib text:', cur_bib_txt)        
        # if cur_bib_txt != "":
           
                # substring = cite_line.count('<ref')


                # cur_line_txt is citation context repeated based on the  number of references
                # cur_line_txt = cite_line_soup.text
                # for i in range(substring):
        for r in range(len(cur_bib_txt)):
        
            citation = cur_line_txt +"\t"+"[{}]".format(extract_authors(soup))+"\t"+ extract_title(soup) +"\t"+"doi:"+extract_doi(soup) +"\t"+ cur_bib_txt[r] + "\n"
        
        # citation = cur_line_txt + "\n" + cur_bib_txt
            citations = citations + citation + "\n"
        
    # citations = citations + citation  + "\n"

    return citations

if __name__=='__main__':


    file_name ='/home/sigma/Downloads/Nanobubbles_Citation_Scripts/script/tei_input/Advanced_Science_2022_Hwang_Monitoring_Wound_Healing_with_Topically_Applied_Optical_NanoFlare_mRNA_Nanosensors.tei'

    with open(file_name,'r') as tei:
        soup = bs(tei, 'xml')
    
    citation = extract_citation(soup)
    with open('citation_output.txt','w') as citation_op:
        citation_op.write(citation)

   