from cmath import nan
from tkinter.ttk import Separator
from xml.sax.handler import property_declaration_handler
# library use for web scraping purposes to pull the data out of html and xml files
# it creates a parse tree from page source code that can be used to extract data in hierarchical and more readable manner.
from bs4 import BeautifulSoup as bs
from soupsieve import select

import common
import bscommon

"""_summary_
the three key relationships in the xml parse tree are:
1. parent: the tag which is used as the reference tag for navigating to child tags.
2. children: the tags contained within the parent tag.
3. siblings: as the name suggests these are the tags exist on the same 
level of the parse tree.

below we nagivate the xml parse tree using the above relationships.

"""

from nltk import tokenize


def parenthesis_split(para_tei,separator=".",lparen="(",rparen=")"):
     
        inner_xml = bscommon.get_inner_xml(para_tei)
        nb_brackets=0
    
        inner_xml = inner_xml.strip(separator) # get rid of leading/trailing seps

        l=[0]
        for i,c in enumerate(inner_xml):
            if c==lparen:
                nb_brackets+=1
            elif c==rparen:
                nb_brackets-=1
            elif c==separator and nb_brackets==0:
                l.append(i)
            # handle malformed string
            # if nb_brackets<0:
            #     raise Exception("Syntax error")

        l.append(len(inner_xml))
        # handle missing closing parentheses
        # if nb_brackets>0:
        #     raise Exception("Syntax error")

        # return([inner_xml[i:j] for i,j in zip(l,l[1:])])
        return([inner_xml[i:j].strip(separator) for i,j in zip(l,l[1:])])
        

# print("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
# print(parenthesis_split("""This is test string. which contains (. inside parantheses). The viral envelope of influenza virus has been used as a carrier for nucleic acids such as siRNA <ref type="bibr" target="#b71">(de Jonge et al., 2006)</ref> and miRNA <ref type="bibr" target="#b181">(Li J. et al., 2013)</ref>. Particularly, the reconstituted influenza virus membrane envelope, called "virosome," acts as an efficient carrier to target small nucleic acid such as siRNA in vitro as well as in vivo (de <ref type="bibr" target="#b71">Jonge et al., 2006)</ref>."""))

# def find_sentece(para_tei):
    
#     return tokenize.sent_tokenize(str(parenthesis_split(para_tei,separator=".",lparen="(",rparen=")")))


def find_cite_lines(para_tei):
    # inner_para = bscommon.get_inner_xml(para_tei)
    # lines = inner_para.split(". ")
    # lines = find_sentece(para_tei)
    lines = parenthesis_split(para_tei,separator=".",lparen="(",rparen=")")
    print(lines)
    # lines = re.split(r'\. (?!\S\)|\() ', inner_para)
    cite_lines = list()
    
    for line in lines:
      
        if '<ref' and 'bibr' in line:
            cite_lines.append(line)
  
            
    return cite_lines

def find_bib_by_ref(bib_ref_list, id):
    bib_txt = ""
    for bib_ref in bib_ref_list:
        if id == bib_ref.get("xml:id"):
            bib_txt = bib_ref.get_text(separator=' ')

            bib_txt = bib_txt.replace("\n", " ")
    return bib_txt

def extract_citation(soup):
    search_list = ["text", "body", "div"]
    cur_search_tei_list = bscommon.xml_get_children(soup, search_list)

    cite_lines = list()
    for cur_search_tei in cur_search_tei_list:
        cur_para_list = bscommon.xml_get_children(cur_search_tei, ["p"])
        for cur_para in cur_para_list:
            cur_cite_lines = find_cite_lines(cur_para)
            cite_lines.extend(cur_cite_lines)

    citations = ""

    search_list = ["listBibl", "biblStruct"]
    bib_ref_list = bscommon.xml_get_children(soup, search_list)

    for cite_line in cite_lines:
        cite_line_soup = bs(cite_line, "lxml")

        cur_line_txt = cite_line_soup.text
        cross_ref_list = cite_line_soup.find_all("ref")
        cur_bib_txt = ""
        
        for cross_ref in cross_ref_list:
            id_str = cross_ref.get("target")
            cur_ref_text = ""
            if id_str != None:
                id_list = id_str.split(" ")
                for id in id_list:
                    id = id.replace("#", "")
                    cur_id_text = find_bib_by_ref(bib_ref_list, id)
                    if cur_id_text !=None:
                        # cur_ref_text = cur_ref_text + "\n" + cur_id_text
                        # new line between the citation removed
                        cur_ref_text = cur_ref_text + cur_id_text
                        # if i remove the \n then there wont be space between citations
                        

            if cur_ref_text != "":
                cur_bib_txt = cur_bib_txt + "\n" + cur_ref_text
        if cur_bib_txt != "":
            citation = cur_line_txt  + cur_bib_txt + "\n"
            # citation = cur_line_txt + "\n" + cur_bib_txt
            # citations = citations + citation + "\n"
            citations = citations + citation + "\n"

    return citations

def get_ip(tei_folder):
    tei_fil_nam_lst = common.get_file_list(tei_folder, "tei")
    
    ip_lst = []
    # ip_lst = ""
    for tei_fil_nam in tei_fil_nam_lst:
        with open(tei_fil_nam, 'r') as tei:
            soup = bs(tei, "xml")
            txt_id = common.gen_rand_txt_id()
            citation = extract_citation(soup)
            ip_lst.append((citation, txt_id))
        
    return ip_lst


if __name__ == "__main__":
    # # process all_teis format in the directory
    # file_name = '/home/sigma/projects/nanobubbles/cqpweb/data/pdf/sna_test_corpus_11_10_2022_sortie_grobid_correct_output/All_teis/1_s2_0_S2452199X21002589_main.tei'
    # # process tei of acs format.
    # file_name = '/home/sigma/projects/nanobubbles/cqpweb/data/pdf/sna_test_corpus_11_10_2022_sortie_grobid_correct_output/acs/nihms_1661145.tei'
    # # process tei of elsevier format.
    # file_name = '/home/sigma/projects/nanobubbles/cqpweb/data/pdf/sna_test_corpus_11_10_2022_sortie_grobid_correct_output/elsevier/1_s2_0_S2452199X21002589_main.tei'
    # # process tei of rsc format.
    # file_name = '/home/sigma/projects/nanobubbles/cqpweb/data/pdf/sna_test_corpus_11_10_2022_sortie_grobid_correct_output/rsc/d0na01005g.tei'
    # # process tei of spring nature format.
    file_name = '/home/sigma/projects/nanobubbles/cqpweb/data/pdf/elsevier_tei_for_test/1_s2_0_S2452199X21002589_main.tei'
    # # process tei of spring nature => autre format.
    # file_name = '/home/sigma/projects/nanobubbles/cqpweb/data/pdf/sna_test_corpus_11_10_2022_sortie_grobid_correct_output/springer_nature/autre/Han2021_Article_SystematicCombinationOfOligonu.tei'
    # # process tei of wiley format.
    # file_name = '/home/sigma/projects/nanobubbles/cqpweb/data/pdf/sna_test_corpus_11_10_2022_sortie_grobid_correct_output/wiley/Advanced_Science_2021_Luo_Ultrasmall_Luminescent_Metal_Nanoparticles_Surface_Engineering_Strategies_for_Biological.tei'
    # # process tei of wiley => autre format.
    # file_name = '/home/sigma/projects/nanobubbles/cqpweb/data/pdf/sna_test_corpus_11_10_2022_sortie_grobid_correct_output/wiley/autre/s43586_022_00104_y.tei'
 

    with open(file_name, 'r') as tei:
        soup = bs(tei, "xml")

        # file_content = parenthesis_split(soup,separator=".",lparen="(",rparen=")")
        file_content = find_cite_lines(soup)
        print(file_content)
        # file_contents = extract_citation(soup)
        # file_contents = get_ip('/home/sigma/pluralsight_project/nanobubbles/cqpweb/data/tei/files_for_sonita_tools_tei')

        # print(file_contents)
        # print(file_contents)
    # with open('/home/sigma/pluralsight_project/nanobubbles/cqpweb/data/tei/files_for_sonita_tools_tei/citation.txt','w') as f:
        # f.write(file_contents)

    
        