"""_summary_
random package needed in order to generate random number for text-id
import string package needed to work with string
glob package short for global is used to return all file paths that match a specific pattern.
like we use global to search for a specific file pattern like eti, xml, txt and ect. 
or search for files where the filename matches a certain pattern by using wildcard characters.
the "os" module provides functions for creating and removing a directory(folder), fetching its contents, changing and identifying the current 
direcotry, etc. we needed to import it to interact with underlying operating syste.
"""

import random
import string
import glob
import os


def gen_rand_txt_id():
    """_summary_
    this function is responsible for generating random text id.

    Returns:
        _type_: return a text id combined string lowercasem uppercase + digits
    """
    chars = string.ascii_lowercase + string.ascii_uppercase + string.digits
    """_summary_
    variable chars store the order of generated random txt id

    Returns:
        _type_: _description_
    """
    rand_txt_id = str()
    """_summary_
    it is an empty string to store random txt id

    """
    for i in range(3):
        """_summary_
        range() produce a sequence of numbers starting from 0 bydefault and increment by 1

        the for-loop in range of sequence start from zero until 3 (excluding 3)
        """
        rand_txt_id_ch = random.choice(chars)
        """_summary_
        randomly choose the three generated id
        """
        rand_txt_id = rand_txt_id + rand_txt_id_ch
        """_summary_
        combining the randomly generated txt it with empty string => store in variable rand_txt_id and return it
       
        """
    return rand_txt_id

# unique text id generator
txt_id_lst = list()
"""_summary_
empty list to store the generated unique txt_id
"""
def get_unique_txt_id():
    """_summary_
    this function is to make sure the generated random txt_id must be unique for each document

    Returns:
        _type_: _description_
    """
    global txt_id_lst
    """_summary_
    ?
    """
    txt_id = gen_rand_txt_id()
    """_summary_
    pass the function gen_rand_txt_id 
    """
    while txt_id in txt_id_lst:
        """_summary_
        if the new txt_id is not in the list of before generated txt_id_lst
        call the function gen_rand_txt_id and store it in txt_id and append it to the list
        then return that unique txt_id
        """
        txt_id = gen_rand_txt_id()
    txt_id_lst.append(txt_id)
    return txt_id

def get_txt_id(pub_id):
    """_summary_
    this function reform the txt_id with special format and return the txt_id
    Args:
        pub_id (_type_): _description_

    Returns:
        _type_: _description_
    """
    txt_id = pub_id.replace(pub_id[3], 'xxpointxx')
    return txt_id
# def get_txt_id(pub_id):
#     """_summary_
#     this function reform the txt_id with special format and return the txt_id
#     Args:
#         pub_id (_type_): _description_

#     Returns:
#         _type_: _description_
#     """
#     txt_id = pub_id.replace(pub_id[3], 'xxpointxx')
#     return txt_id

def write_to_file(obj, file_name):
    """_summary_
    this function with two parameters obj and file_name
    the parameter obj
    the parameter file_name

    Args:
        obj (_type_): 
        file_name (_type_): take the file_name and write to it the object (obj) string
    """
    obj_str = str(obj)
    with open(file_name, "w") as f:
        """_summary_
        open the file and write to it object string
        """
        f.write(obj_str)

def append_to_file(obj, file_name):
    """_summary_
    this function append the obje string to the file

    Args:
        obj (_type_): object string
        file_name (_type_): the file which we append strings to it
    """
    obj_str = str(obj)
    """_summary_
    
    """
    with open(file_name, "a") as f:
        f.write(obj_str)

def get_op_fil_nam_fil(ip_path):
    """_summary_
    this function take the name of ip_path and add the extension .abs, .cont and ect
    then store respectively each of them in a variabl like abs_op_file_name, cont_op_file_name ect. and return
    those variable

    Args:
        ip_path (_type_): take the text of input_folder and add to it extensions

    Returns:
        _type_: the variable contains abs, cont, cite and meta data
    """
    abs_op_fil_nam = os.path.splitext(ip_path)[0] + '.abs'
    cont_op_fil_nam = os.path.splitext(ip_path)[0] + '.cont'
    # cite_op_fil_nam = os.path.splitext(ip_path)[0] + '.cite'
    meta_op_fil_nam = os.path.splitext(ip_path)[0] + '.meta'
    return (abs_op_fil_nam, cont_op_fil_nam, meta_op_fil_nam)

def get_op_fil_nam_dir(ip_path):
    """_summary_
    this function get the directory name of output file

    Args:
        ip_path (_type_): _description_

    Returns:
        _type_: _description_
    """
    ip_dir_nam = os.path.basename(ip_path)
    """_summary_
    get the main text of ip_path

    Returns:
        _type_: _description_
    """
    abs_op_fil_nam = os.path.join(ip_path, ip_dir_nam + '.abs')
    # cont_op_fil_nam = os.path.join(ip_path, ip_dir_nam + '.cont')
    # cite_op_fil_nam = os.path.join(ip_path, ip_dir_nam + '.cite')
    meta_op_fil_nam = os.path.join(ip_path, ip_dir_nam + '.meta')
    # return (abs_op_fil_nam, cont_op_fil_nam, meta_op_fil_nam)
    return (abs_op_fil_nam, meta_op_fil_nam)


def get_file_list(folder, ext):
    """_summary_
    this function get list of file from folder with ext

    Args:
        folder (_type_): _description_
        ext (_type_): 
    """
    os.chdir(folder)
    """_summary_
    os.chdir check if the folder is directory or not
    Raises:
        Exception: _description_
    """
    fil_nam_lst = list(glob.glob('*.{}'.format(ext)))
    """_summary_
    glop return file path add ext to it and keeping in a list
    """
    if len(fil_nam_lst) == 0:
        """_summary_
        check the file_nam_lst if it == 0 means no file and raise exception with message no teil file other wise return list of file
        """
        raise Exception('No tei files present')
    return fil_nam_lst


def get_file_ext(folder):
    """
    this function get fine extension
    """
    tei_fil_nam_lst = []
    xml_fil_nam_lst = []
    try:
        tei_fil_nam_lst = get_file_list(folder, "tei")
    except Exception:
        xml_fil_nam_lst = get_file_list(folder, "xml")
    finally:
        if len(tei_fil_nam_lst) == 0 and len(xml_fil_nam_lst) == 0:
            raise Exception('No supported file types present')
        if len(tei_fil_nam_lst) != 0 and len(xml_fil_nam_lst) != 0:
            raise Exception('Do not know which supported file type to pick')
        
    if len(tei_fil_nam_lst) != 0:
        ret_type = "tei"
    if len(xml_fil_nam_lst) != 0:
        ret_type = "xml"
    return ret_type

if __name__=='__main__':
    txtid = get_unique_txt_id()
    print(txtid)

