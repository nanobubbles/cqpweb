import os
import spacy

import common, parse_tei as tei, parse_xlsx as xlsx, parse_xml as xml

def get_abs_op(sp, abstract ,cont, txt_id):
 
    vrt_sentences = str()
    # this line added
    combine_abs_cont = ''
    combine_abs_cont = abstract + cont + "\n"
    sp_abs = sp(combine_abs_cont)
    # this line added
    # sp_cont = sp(cont)
    # concatenate absract and cont
    # for sent in combine_abs_cont.sents:
    for sent in sp_abs.sents:
        word_lst = sp(sent.text)
        pos_sentence = str()
        for word in word_lst:
            pos_word = '{}\t{}\t{}\t{}\n'.format(word.text, word.pos_, word.lemma_, word.ent_type_)
            pos_sentence = pos_sentence + pos_word
        vrt_sentence = '<s>\n{}</s>\n'.format(pos_sentence)
        vrt_sentences = vrt_sentences + vrt_sentence
    # vrt_output = '<text id = "{}">\n{}</text>\n'.format(txt_id, vrt_sentences)
    vrt_output = '<text id = "{}">\n{}</text>\n'.format(txt_id, vrt_sentences)
  
    return vrt_output

"""_summary_
for extracting abstract and content in single file
i pass cont as parameter to function get_abs_op
and i comment this function
"""

# def get_cont_op(sp, cont, txt_id):
#     return get_abs_op(sp, cont, txt_id)

# def get_cite_op(citation_lst, txt_id):
#     vrt_output = ""
#     for citation in citation_lst:
#         vrt_sentences = str()
#         sp_cite = sp(citation)
#         for sent in sp_cite.sents:
#             word_lst = sp(sent.text)
#             pos_sentence = str()
#             for word in word_lst:
#                 pos_word = '{}\t{}\t{}\t{}\n'.format(word.text, word.pos_, word.lemma_, word.ent_type_)
#                 pos_sentence = pos_sentence + pos_word
#             vrt_sentences = vrt_sentences + pos_sentence

#         vrt_output = '{}<s>\n{}</s>\n'.format(vrt_output, vrt_sentences)
#     vrt_output = '<text id = "{}">\n{}</text>\n'.format(txt_id, vrt_output)
#     return vrt_output

def get_meta_op(txt_id, title, authors, author_affiliation, publisher, pub_yr, abstract):
   
    meta_sentence = '{}\t{}\t{}\t{}\t{}\t{}\t{}\n'.format(txt_id, title, authors, author_affiliation, publisher, pub_yr, abstract)
    
    return meta_sentence


def calc_op(ip_lst):
    sp = spacy.load('en_ner_bc5cdr_md')
    # sp = spacy.load('en_core_sci_md')
    # sp = spacy.load('en_core_web_sm')
    abs_op = ''
    # cont_op = ''
    # cite_op = ''
    meta_op = ''
    for (txt_id, abstract, cont, title, authors, author_affiliation, publisher, pub_yr) in ip_lst:
        abs_op = abs_op + get_abs_op(sp, abstract, cont, txt_id)
        # print("====================abs_op==========", abs_op)
       
        # i comment it because i passed cont to abstract functon
        # cont_op = cont_op + get_cont_op(sp, cont, txt_id)
        # cite_op = cite_op + get_cite_op(sp, cite, txt_id)
        # meta_op = meta_op + get_meta_op(txt_id, title, authors,author_affiliation, publisher, pub_yr, abstract)
        meta_op = meta_op + get_meta_op(txt_id, title, authors,author_affiliation, publisher, pub_yr, abstract)
        # print("=========meta_op==============", meta_op)

    # because i passed the variable cont which has data of content to abstract
    # return (abs_op, cont_op, meta_op)

    return (abs_op, meta_op)
    

def gen_op(ip_path):
    if os.path.isdir(ip_path):
        # because combined cont file with abstract file se we do the following changes
        # (abs_op_fil_nam, cont_op_fil_nam,meta_op_fil_nam) = common.get_op_fil_nam_dir(ip_path)
        (abs_op_fil_nam,meta_op_fil_nam) = common.get_op_fil_nam_dir(ip_path)
        ext = common.get_file_ext(ip_path)
        if ext == "tei":
            ip_lst = tei.get_ip(ip_path)
        elif ext == "xml":
            ip_lst = xml.get_ip(ip_path)
        else:
            raise Exception('Do not recognize the file format')
    elif os.path.isfile(ip_path):
        (abs_op_fil_nam, meta_op_fil_nam) = common.get_op_fil_nam_fil(ip_path)
        if ip_path.lower().endswith('.xlsx'):
            ip_lst = xlsx.get_ip(ip_path)
        else:
            raise Exception('Do not recognize the file format')
    else:
        raise Exception('Invalid path')

    # (abs_op, cont_op, meta_op) = calc_op(ip_lst)
    (abs_op, meta_op) = calc_op(ip_lst)

    # with open(abs_op_fil_nam, 'w') as abs_fil:
    #     abs_fil.write(abs_op)

    with open(abs_op_fil_nam, 'w') as abs_fil:
        abs_fil.write(abs_op)

    """_summary_
    because i combined cont content to abstract so i write both in tei.abs
    i comment the below file
    """
    # with open(cont_op_fil_nam, 'w') as cont_fil:
    #     cont_fil.write(cont_op)

    # with open(cite_op_fil_nam, 'w') as cite_fil:
    #     cite_fil.write(cite_op)

    with open(meta_op_fil_nam, 'w') as meta_fil:
        meta_fil.write(meta_op)

def success_ex():
    return "end of excution"



if __name__ == "__main__":
    # xlsx_fil_nam = '/home/sigma/nanobubbles/script/data/xlsx/Dimensions-Publication.xlsx'
    tei_folder = '/home/sigma/Downloads/Nanobubbles_Extract_Tei_Content_Script/script/tei_output'
    # xml_folder = '/home/sigma/nanobubbles/script/cqpweb/Script/data/xml'
    ip_path = tei_folder
    gen_op(ip_path)
    # abscont= get_abs_op(sp, abstract ,cont, txt_id)
    
