# """An example program that uses the elsapy module"""

from elsapy.elsclient import ElsClient
from elsapy.elsprofile import ElsAuthor, ElsAffil
from elsapy.elsdoc import FullDoc, AbsDoc
from elsapy.elssearch import ElsSearch
import json
    
key = "403d3153071030688f2f3b483c2507b4"

import urllib.request
import shutil
import os
import re


def extractURLs(fileContent):
     urls = re.findall('\(http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|     [!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', fileContent.lower())
     return urls

finalurl=[]

myFile = open('/home/sigma/nanobubbles/cqpweb/elsapy/ScienceDirect_citations_1652255131457.txt',encoding="utf8")
fileContent = myFile.read()
finalurl=(extractURLs(fileContent))

# print(finalurl)

for url in finalurl:
  name = url.rsplit('/', 1)[-1][0:-1]
  response,headers =   urllib.request.urlretrieve('https://api.elsevier.com/content/article/pii/'+name+'?apiKey='+key)
  filename = os.path.join('/home/sigma/nanobubbles/cqpweb/elsapy', response)
  shutil.copy(filename, '/home/sigma/nanobubbles/cqpweb/elsapy'+ name +".xml")