from cmath import nan
from http import client
from textwrap import indent
import types
import pandas as pd
import numpy as np
import glob, os
from bs4 import BeautifulSoup as bs
import spacy
import requests
import grobid_tei_xml

from grobid_client.grobid_client import GrobidClient

import re

# pdf_resp = "/home/sigma/nanobubbles/script/cqpweb/Script/data/pdf"

# client = GrobidClient(config_path="/home/sigma/grobid_client_python/config.json")

client = GrobidClient(config_path="/home/sigma/grobid_client_python/config.json")

# client.process("processFulltextDocument", "/home/sigma/pluralsight_project/nanobubbles/cqpweb/data/pdf", n=15)

client.process("processFulltextDocument", "/home/sigma/projects/nanobubbles/cqpweb/data/pdf/Sna_test_corputs_11_10_2022/wiley", "/home/sigma/projects/nanobubbles/cqpweb/data/pdf/Sna_test_corputs_11_10_2022/wiley" , n=15)


# content = []

# with open("/home/sigma/nanobubbles/script/output_tei/Mirkin.tei", "r") as tei_file:

#     content = tei_file.readlines()
#     content = "".join(content)
#     bs_content = bs(content, "lxml")

# result = bs_content.find_all("author")

# print("the author of this paper are", result)
# author_soup = bs_content.select('teiHeader')[0].select('filedesc')[0].select('sourcedesc')[0].select('biblstruct')[0].select('analytic')[0]
# authors = []
# auth = ""
# for au in author_soup.find_all("author"):
    # print("author text: ", "{} {} ({})".format(au.surname.text, au.forename.text, au.orgname.text))
#     all_authors = "{} {} {}({})".format(au.surname.text, au.forename.text, au.orgname.next_element.text, au.orgname.text)
#     auth = auth + all_authors
# print(auth)
    # authors.append(all_authors.replace('\n', ' ').strip())
    # print(*authors, sep='')

# idno = bs_content.find('idno').text
# print(idno)

# ref_soup = bs_content.select('text')[0].select('body')[0].select('div')[0].select('p')[0]
# ref_result = ref_soup.find('ref')

# text = "Sinus node dysfunction (SND) is a relatively common problem after heart transplantation especially in the early postoperative period.' It explains the majority of the early bradyarrhythmias. The most common predisposing factor remains direct surgical trauma or associated disruption of the blood supply to the sinus node.' In contrast, atrioventricular blocks are more common than SND as a cause of bradyarrhythmias occurring in the late postoperative period.3 This report describes a heart transplant patient 3 years after his surgery, who presented with recurrent dizzy spells and presyncopal episodes for 3 months and then syncopal episodes in the presence of an acute myocardial infarction (AMI). The patient required emergency temporary and subsequent permanent pacemaking for SND presumably due to severe sinus node artery obstruction."

# print(type(text))

# text = text = "Trondheim is a small city with a university and 140000 inhabitants. Its central bus systems has 42 bus lines, serving 590 stations, with 1900 (departures per) day in average. T h a t gives approximately 60000 scheduled bus station passings per day, which is somehow represented in the route data base. The starting point is to automate the function (Garry Weber, 2005) of a route information agent."


# print (re.findall(r"([^.]*?\(.+ [0-9]+\)[^.]*\.)",text))



    
# m = re.search('(?<=abc)def', 'abcdef')
# print(m.group(0))

# m = re.search(r'(?<=-)\w+', 'spam-egg')
# print(m.group())