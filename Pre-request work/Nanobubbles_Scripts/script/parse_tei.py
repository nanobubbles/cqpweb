# extract citation context from main content
# this library give access to mathematical functions for complex numbers.
"""_summary_
for stefan project
"""
from cmath import nan
from tkinter.ttk import Separator
from xml.sax.handler import property_declaration_handler
# library use for web scraping purposes to pull the data out of html and xml files
# it creates a parse tree from page source code that can be used to extract data in hierarchical and more readable manner.
from bs4 import BeautifulSoup as bs
from soupsieve import select

import common
import bscommon
from nltk import tokenize
# def delete_citation(cur_tei):
#     cur_tei_wo_citations = bscommon.delete_cur_xml_tag(cur_tei, "ref")
#     return cur_tei_wo_citations

def extract_abstract(soup):
    abs_str = bscommon.get_element(soup, 'abstract')
    return abs_str

# copied content from script 1

def extract_file_contents(soup):
    search_list = ["text", "body", "div"]
    section_tei_list = bscommon.xml_get_children(soup, search_list)

    file_str = str()
    for cur_section_tei in section_tei_list:
        cur_section_str = str()
        cur_para_tei_list = bscommon.xml_get_children(cur_section_tei, ["p"])
        for cur_para_tei in cur_para_tei_list:
            cur_para_str = str()
            inner_para = bscommon.get_inner_xml(cur_para_tei)
            lines = inner_para.split(". ")
            for line in lines:
                if not '<ref' or not 'bibr' in line:
                    line_soup = bs(line, "lxml")
                    line_str = line_soup.text.strip()

                    if line_str != "":
                        cur_para_str = cur_para_str + line_str + ". "

            if cur_para_str != "":
                cur_section_str = cur_section_str + cur_para_str + "\n"

        if cur_section_str != "":
            file_str = file_str + cur_section_str + "\n"

    return file_str


def parenthesis_split(para_tei,separator=".",lparen="(",rparen=")"):
     
        inner_xml = bscommon.get_inner_xml(para_tei)
        nb_brackets=0
    
        inner_xml = inner_xml.strip(separator) # get rid of leading/trailing seps

        l=[0]
        for i,c in enumerate(inner_xml):
            if c==lparen:
                nb_brackets+=1
            elif c==rparen:
                nb_brackets-=1
            elif c==separator and nb_brackets==0:
                l.append(i)
            # handle malformed string
            # if nb_brackets<0:
            #     raise Exception("Syntax error")

        l.append(len(inner_xml))
        # handle missing closing parentheses
        # if nb_brackets>0:
        #     raise Exception("Syntax error")

        # return([inner_xml[i:j] for i,j in zip(l,l[1:])])
        return([inner_xml[i:j].strip(separator) for i,j in zip(l,l[1:])])

def find_cite_lines(para_tei):
    # inner_para = bscommon.get_inner_xml(para_tei)
    # lines = inner_para.split(". ")
    # lines = find_sentece(para_tei)
    lines = parenthesis_split(para_tei,separator=".",lparen="(",rparen=")")
    print(lines)
    # lines = re.split(r'\. (?!\S\)|\() ', inner_para)
    cite_lines = list()
    
    for line in lines:
      
        if '<ref' and 'bibr' in line:
            cite_lines.append(line)
  
            
    return cite_lines

def find_bib_by_ref(bib_ref_list, id):
    bib_txt = ""
    for bib_ref in bib_ref_list:
        if id == bib_ref.get("xml:id"):
            # bib_txt = bib_ref.text
            bib_txt = bib_ref.get_text(separator=' ')
            bib_txt = bib_txt.replace("\n", " ")
    return bib_txt

def extract_citation(soup):
    search_list = ["txt", "body", "div"]
    cur_search_tei_list = bscommon.xml_get_children(soup, search_list)

    cite_lines = list()
    for cur_search_tei in cur_search_tei_list:
        cur_para_list = bscommon.xml_get_children(cur_search_tei, ["p"])
        for cur_para in cur_para_list:
            cur_cite_lines = find_cite_lines(cur_para)
            cite_lines.extend(cur_cite_lines)

    citations = list()

    search_list = ["listBibl", "biblStruct"]
    bib_ref_list = bscommon.xml_get_children(soup, search_list)

    for cite_line in cite_lines:
        cite_line_soup = bs(cite_line, "lxml")
        cur_line_txt = cite_line_soup.text
        cross_ref_list = cite_line_soup.find_all("ref")
        cur_bib_txt = ""

        for cross_ref in cross_ref_list:
            id_str = cross_ref.get("target")
            cur_ref_text = ""
            if id_str != None:
                id_list = id_str.split(" ")
                for id in id_list:
                    id = id.replace("#", "")
                    cur_id_text = find_bib_by_ref(bib_ref_list, id)
                    if cur_id_text != None:
                        cur_ref_text = cur_ref_text + "\n" + cur_id_text
            if cur_ref_text != "":
                cur_bib_txt = cur_bib_txt + "\n" + cur_ref_text
        if cur_bib_txt != "":
            citation = cur_line_txt + "\n" + cur_bib_txt
            citations.append(citation)

    return citations

# original
def extract_author_affiliation(soup):
    # author_affiliation = []
    author_affiliation = ''

    try:
        file_soup = soup.select('teiHeader')[0].select('fileDesc')[0].select('sourceDesc')[0].select('biblStruct')[0].select('analytic')[0]
        author_soup_lst = file_soup.find_all('affiliation')
        print(author_soup_lst)
    except Exception:
        pass
    #af_author_lst = []
    # print("==============================")
    for i in author_soup_lst:
        # print(i)
        # print("*************************")
        # author_affiliation.append((i.get_text().strip()).replace("\n", " "))
        author_affiliation = author_affiliation + ((i.get_text().strip()).replace("\n", " ")) + ","

    return author_affiliation

# # ***************************************************************************
def extract_authors(soup):

    authors = ''
    try:

        file_soup = soup.select('teiHeader')[0].select('fileDesc')[0].select('sourceDesc')[0].select('biblStruct')[0].select('analytic')[0]
        author_soup_lst = file_soup.find_all('persName')
    
    except Exception:
        pass

    for author_soup in author_soup_lst:

        surname = author_soup.surname.get_text().strip()
        firstname = author_soup.forename.get_text().strip()
        authors = authors + '{} {}'.format(surname,firstname) + ","

    return authors


# # *****************************************************************************
# # ===================================extract title=============================
def extract_title(soup):
    title = nan
    try:
        title_soup = soup.select('teiHeader')[0].select('fileDesc')[0].select('titleStmt')[0]
        title = bscommon.get_element(title_soup,'title')
        
    except Exception:
        pass

    return title

# # ===================================end of extract title======================

def extract_publisher(soup):
    publisher = nan
    try:
        publisher_soup = soup.select('teiHeader')[0].select('fileDesc')[0].select('publicationStmt')[0]
        publisher = bscommon.get_element(publisher_soup, 'publisher')
    except Exception:
        pass
    return publisher

def extract_pubyr(soup):
    pub_yr = nan
    try:
        publisher_soup = soup.select('teiHeader')[0].select('fileDesc')[0].select('publicationStmt')[0]
        pub_yr = bscommon.get_element(publisher_soup, 'date')
    except Exception:
        pass
    return pub_yr

def get_ip(tei_folder):
    tei_fil_nam_lst = common.get_file_list(tei_folder, "tei")
    ip_lst = []
    for tei_fil_nam in tei_fil_nam_lst:
        with open(tei_fil_nam, 'r') as tei:
            soup = bs(tei, "xml")

            txt_id = common.get_unique_txt_id()

            abstract = extract_abstract(soup)
            file_cont = extract_file_contents(soup)
            # citation = extract_citation(soup)
            title = extract_title(soup)
            authors = extract_authors(soup)
            author_affiliation = extract_author_affiliation(soup)
            publisher = extract_publisher(soup)
            pub_yr = extract_pubyr(soup)
            
            ip_lst.append((txt_id, abstract, file_cont, title, authors, author_affiliation, publisher, pub_yr))

    return ip_lst


if __name__ == "__main__":
   
    file_name = '/home/sigma/Downloads/Nanobubbles_Extract_Tei_Content_Script/script/tei_input/d0na01005g.tei'
  
    with open(file_name, 'r') as tei:
        soup = bs(tei, "xml")

