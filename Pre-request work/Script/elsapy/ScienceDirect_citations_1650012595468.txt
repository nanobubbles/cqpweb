Alena Nelaeva, Frode Nilssen,
Contrasting knowledge development for internationalization among emerging and advanced economy firms: A review and future research,
Journal of Business Research,
Volume 139,
2022,
Pages 232-256,
ISSN 0148-2963,
https://doi.org/10.1016/j.jbusres.2021.09.053.
(https://www.sciencedirect.com/science/article/pii/S0148296321007049)
Abstract: Knowledge development—an integral part of firms’ internationalization—has generated a considerable amount of research on how firms from emerging and advanced countries acquire, integrate, and utilize knowledge. Despite theoretical justifications for the context significance for internationalization, the existing literature is fragmented and overlooks whether and how firms originating in distinct countries differ in their knowledge development processes. Relying on the antecedents, decisions, and outcomes, and the theory, context, characteristics, and methodology frameworks, we bridge this gap by reviewing 81 papers published in leading journals since 2007. Our paper extends the literature by classifying knowledge development during firm internationalization from emerging and advanced countries. We show how environmental and status differences create diverse effects on the type of knowledge firms seek, the ways they acquire and integrate it, and the consequent internationalization decisions. Among notable further directions, we emphasize shifting focus from organization- to individual-level learning in diverse organizational contexts.
Keywords: Knowledge development; Internationalization; Organizational learning; Literature review; Emerging markets; Advanced economies
