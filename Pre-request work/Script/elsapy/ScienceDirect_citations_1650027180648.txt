Zoltán Kmetty, Róbert Tardos,
Party Nexus Position Generator: New tool for the measurement of political homophily and political network diversity,
Social Networks,
Volume 70,
2022,
Pages 112-125,
ISSN 0378-8733,
https://doi.org/10.1016/j.socnet.2021.11.011.
(https://www.sciencedirect.com/science/article/pii/S0378873321001088)
Abstract: The role of networks has been growing attention in recent decades in explaining political behaviour. Political nexus aspects also get on the agenda in studying various resources of status attainment. Despite the general realization of these relevant network implications, some conceptual and measurement issues are still debatable. In this paper, we introduce a new tool for measuring political acquaintanceship networks, the Party Nexus Position Generator (PNPG). We will show how one of the most widely used SNA-instruments, the technique of position generator, could be transformed to apply for the measurement of political networks. We tested the tool in two countries, Germany and Hungary, with surveys administered by different methods: online and face-to-face. The presentation of findings on German and Hungarian political networks may help us understand how the broader settings affect the composition of political networks and their influences on political behavior. Results from two different countries may also contribute to assess the validity of the PNPG tools introduced by our study.
Keywords: Party Nexus Position Generator; Political nexus diversity; Political homophily; Validity; Hungary; Germany
