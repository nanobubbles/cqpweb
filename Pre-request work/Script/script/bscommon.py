from cmath import nan
from bs4 import BeautifulSoup as bs

def get_element(soup, node_name):
    ele = nan
    try:
        ele = getattr(soup, node_name).getText().strip()
    except Exception:
        pass
    return ele

def delete_cur_xml_tag(cur_xml, delete_tag):
    cur_xml_str = str(cur_xml) # by creating a string and then re-creating a new soup we are effectively doing a copy
    # this is needed coz in python everything is a reference and modifying it directly here will change the original soup
    cur_xml_soup = bs(cur_xml_str, "lxml")
    delete_xmls = cur_xml_soup.find_all(delete_tag)
    for delete_xml in delete_xmls:
        delete_xml.decompose()

    return cur_xml_soup.find("body").next

def xml_get_child(root_xml, search_tag_list):
    cur_search_xml = root_xml
    for search in search_tag_list:
        cur_search_xml = cur_search_xml.find(search)
    return cur_search_xml

def xml_get_children(root_xml, search_tag_list):
    cur_search_xml = root_xml
    for (idx, search) in enumerate(search_tag_list):
        if idx == len(search_tag_list) - 1:
            cur_search_xml = cur_search_xml.find_all(search)
        else:
            cur_search_xml = cur_search_xml.find(search)
    return cur_search_xml

def get_inner_xml(root_xml):
    return root_xml.encode_contents().decode()
