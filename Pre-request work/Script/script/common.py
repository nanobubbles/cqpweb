import random
import string
import glob
import os

def gen_rand_txt_id():
    chars = string.ascii_lowercase + string.ascii_uppercase + string.digits
    rand_txt_id = str()
    for i in range(3):
        rand_txt_id_ch = random.choice(chars)
        rand_txt_id = rand_txt_id + rand_txt_id_ch
    return rand_txt_id

# unique text id generator
txt_id_lst = list()
def get_unique_txt_id():
    global txt_id_lst
    txt_id = gen_rand_txt_id()
    while txt_id in txt_id_lst:
        txt_id = gen_rand_txt_id()
    txt_id_lst.append(txt_id)
    return txt_id

def get_txt_id(pub_id):
    txt_id = pub_id.replace(pub_id[3], 'xxpointxx')
    return txt_id

def write_to_file(obj, file_name):
    obj_str = str(obj)
    with open(file_name, "w") as f:
        f.write(obj_str)

def append_to_file(obj, file_name):
    obj_str = str(obj)
    with open(file_name, "a") as f:
        f.write(obj_str)

def get_op_fil_nam_fil(ip_path):
    abs_op_fil_nam = os.path.splitext(ip_path)[0] + '.abs'
    cont_op_fil_nam = os.path.splitext(ip_path)[0] + '.cont'
    cite_op_fil_nam = os.path.splitext(ip_path)[0] + '.cite'
    meta_op_fil_nam = os.path.splitext(ip_path)[0] + '.meta'
    return (abs_op_fil_nam, cont_op_fil_nam, cite_op_fil_nam, meta_op_fil_nam)

def get_op_fil_nam_dir(ip_path):
    ip_dir_nam = os.path.basename(ip_path)
    abs_op_fil_nam = os.path.join(ip_path, ip_dir_nam + '.abs')
    cont_op_fil_nam = os.path.join(ip_path, ip_dir_nam + '.cont')
    cite_op_fil_nam = os.path.join(ip_path, ip_dir_nam + '.cite')
    meta_op_fil_nam = os.path.join(ip_path, ip_dir_nam + '.meta')
    return (abs_op_fil_nam, cont_op_fil_nam, cite_op_fil_nam, meta_op_fil_nam)


def get_file_list(folder, ext):
    os.chdir(folder)
    fil_nam_lst = list(glob.glob('*.{}'.format(ext)))
    if len(fil_nam_lst) == 0:
        raise Exception('No tei files present')
    return fil_nam_lst


def get_file_ext(folder):
    tei_fil_nam_lst = []
    xml_fil_nam_lst = []
    try:
        tei_fil_nam_lst = get_file_list(folder, "tei")
    except Exception:
        xml_fil_nam_lst = get_file_list(folder, "xml")
    finally:
        if len(tei_fil_nam_lst) == 0 and len(xml_fil_nam_lst) == 0:
            raise Exception('No supported file types present')
        if len(tei_fil_nam_lst) != 0 and len(xml_fil_nam_lst) != 0:
            raise Exception('Do not know which supported file type to pick')
        
    if len(tei_fil_nam_lst) != 0:
        ret_type = "tei"
    if len(xml_fil_nam_lst) != 0:
        ret_type = "xml"
    return ret_type
