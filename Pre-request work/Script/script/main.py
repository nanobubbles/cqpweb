import os
import spacy

import common, parse_tei as tei, parse_xlsx as xlsx, parse_xml as xml

def get_abs_op(sp, abstract, txt_id):
    vrt_sentences = str()
    sp_abs = sp(abstract)
    for sent in sp_abs.sents:
        word_lst = sp(sent.text)
        pos_sentence = str()
        for word in word_lst:
            pos_word = '{}\t{}\t{}\t{}\n'.format(word.text, word.pos_, word.lemma_, word.ent_type_)
            pos_sentence = pos_sentence + pos_word
        vrt_sentence = '<s>\n{}</s>\n'.format(pos_sentence)
        vrt_sentences = vrt_sentences + vrt_sentence
    vrt_output = '<text id = "{}">\n{}</text>\n'.format(txt_id, vrt_sentences)
    return vrt_output

def get_cont_op(sp, cont, txt_id):
    return get_abs_op(sp, cont, txt_id)

def get_cite_op(sp, citation_lst, txt_id):
    vrt_output = ""
    for citation in citation_lst:
        vrt_sentences = str()
        sp_cite = sp(citation)
        for sent in sp_cite.sents:
            word_lst = sp(sent.text)
            pos_sentence = str()
            for word in word_lst:
                pos_word = '{}\t{}\t{}\t{}\n'.format(word.text, word.pos_, word.lemma_, word.ent_type_)
                pos_sentence = pos_sentence + pos_word
            vrt_sentences = vrt_sentences + pos_sentence

        vrt_output = '{}<s>\n{}</s>\n'.format(vrt_output, vrt_sentences)
    vrt_output = '<text id = "{}">\n{}</text>\n'.format(txt_id, vrt_output)
    return vrt_output

def get_meta_op(txt_id, author_affiliation, publisher, pub_yr, abstract):
    meta_sentence = '{}\t{}\t{}\t{}\t{}\n'.format(txt_id, author_affiliation, publisher, pub_yr, abstract)
    return meta_sentence

def calc_op(ip_lst):
    sp = spacy.load('en_ner_bc5cdr_md')
    # sp = spacy.load('en_core_sci_md')
    # sp = spacy.load('en_core_web_sm')
    abs_op = ''
    cont_op = ''
    cite_op = ''
    meta_op = ''
    for (abstract, cont, cite, txt_id, author_affiliation, publisher, pub_yr) in ip_lst:
        abs_op = abs_op + get_abs_op(sp, abstract, txt_id)
        cont_op = cont_op + get_cont_op(sp, cont, txt_id)
        cite_op = cite_op + get_cite_op(sp, cite, txt_id)
        meta_op = meta_op + get_meta_op(txt_id, author_affiliation, publisher, pub_yr, abstract)
    return (abs_op, cont_op, cite_op, meta_op)

def gen_op(ip_path):
    if os.path.isdir(ip_path):
        (abs_op_fil_nam, cont_op_fil_nam, cite_op_fil_nam, meta_op_fil_nam) = common.get_op_fil_nam_dir(ip_path)
        ext = common.get_file_ext(ip_path)
        if ext == "tei":
            ip_lst = tei.get_ip(ip_path)
        elif ext == "xml":
            ip_lst = xml.get_ip(ip_path)
        else:
            raise Exception('Do not recognize the file format')
    elif os.path.isfile(ip_path):
        (abs_op_fil_nam, cont_op_fil_nam, cite_op_fil_nam, meta_op_fil_nam) = common.get_op_fil_nam_fil(ip_path)
        if ip_path.lower().endswith('.xlsx'):
            ip_lst = xlsx.get_ip(ip_path)
        else:
            raise Exception('Do not recognize the file format')
    else:
        raise Exception('Invalid path')

    (abs_op, cont_op, cite_op, meta_op) = calc_op(ip_lst)

    with open(abs_op_fil_nam, 'w') as abs_fil:
        abs_fil.write(abs_op)

    with open(cont_op_fil_nam, 'w') as cont_fil:
        cont_fil.write(cont_op)

    with open(cite_op_fil_nam, 'w') as cite_fil:
        cite_fil.write(cite_op)

    with open(meta_op_fil_nam, 'w') as meta_fil:
        meta_fil.write(meta_op)


if __name__ == "__main__":
    # tei_folder = '/home/sigma/nanobubbles/script/data/tei'
    # xlsx_fil_nam = '/home/sigma/nanobubbles/script/data/xlsx/Dimensions-Publication.xlsx'
    tei_folder2 = '/home/sigma/nanobubbles/script/data/tei2'
    # xml_folder = '/home/sigma/nanobubbles/script/data/xml'
    ip_path = tei_folder2
    gen_op(ip_path)
