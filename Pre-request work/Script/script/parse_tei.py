# extract citation context from main content
from cmath import nan
from bs4 import BeautifulSoup as bs

import common
import bscommon

def delete_citation(cur_tei):
    cur_tei_wo_citations = bscommon.delete_cur_xml_tag(cur_tei, "ref")
    return cur_tei_wo_citations

def extract_file_contents(soup):
    search_list = ["text", "body", "div"]
    section_tei_list = bscommon.xml_get_children(soup, search_list)

    file_str = str()
    for cur_section_tei in section_tei_list:
        cur_section_str = str()
        cur_para_tei_list = bscommon.xml_get_children(cur_section_tei, ["p"])
        for cur_para_tei in cur_para_tei_list:
            cur_para_str = str()
            inner_para = bscommon.get_inner_xml(cur_para_tei)
            lines = inner_para.split(". ")
            for line in lines:
                if not '<ref' or not 'bibr' in line:
                    line_soup = bs(line, "lxml")
                    line_str = line_soup.text.strip()

                    if line_str != "":
                        cur_para_str = cur_para_str + line_str + ". "

            if cur_para_str != "":
                cur_section_str = cur_section_str + cur_para_str + "\n"

        if cur_section_str != "":
            file_str = file_str + cur_section_str + "\n"

    return file_str
    # search_list = ["text", "body", "div"]
    # file_cont_list = bscommon.xml_get_children(soup, search_list)
    # file_str = ""
    # for file_cont in file_cont_list:
    #     file_cont_wo_cite = delete_citation(file_cont)
    #     file_str = file_str + "\n" + file_cont_wo_cite.text
    # return file_str

def extract_abstract(soup):
    abs_str = bscommon.get_element(soup, 'abstract')
    return abs_str

def find_cite_lines(para_tei):
    inner_para = bscommon.get_inner_xml(para_tei)
    lines = inner_para.split(". ")
    cite_lines = list()
    for line in lines:
        if '<ref' and 'bibr' in line:
            cite_lines.append(line)

    return cite_lines

def find_bib_by_ref(bib_ref_list, id):
    bib_txt = ""
    for bib_ref in bib_ref_list:
        if id == bib_ref.get("xml:id"):
            bib_txt = bib_ref.text
            bib_txt = bib_txt.replace("\n", " ")
    return bib_txt

def extract_citation(soup):
    search_list = ["text", "body", "div"]
    cur_search_tei_list = bscommon.xml_get_children(soup, search_list)

    cite_lines = list()
    for cur_search_tei in cur_search_tei_list:
        cur_para_list = bscommon.xml_get_children(cur_search_tei, ["p"])
        for cur_para in cur_para_list:
            cur_cite_lines = find_cite_lines(cur_para)
            cite_lines.extend(cur_cite_lines)

    citations = list()

    search_list = ["listBibl", "biblStruct"]
    bib_ref_list = bscommon.xml_get_children(soup, search_list)
    for cite_line in cite_lines:
        cite_line_soup = bs(cite_line, "lxml")
        cur_line_txt = cite_line_soup.text
        cross_ref_list = cite_line_soup.find_all("ref")
        cur_bib_txt = ""
        for cross_ref in cross_ref_list:
            id_str = cross_ref.get("target")
            cur_ref_text = ""
            if id_str != None:
                id_list = id_str.split(" ")
                for id in id_list:
                    id = id.replace("#", "")
                    cur_id_text = find_bib_by_ref(bib_ref_list, id)
                    if cur_id_text != None:
                        cur_ref_text = cur_ref_text + "\n" + cur_id_text
            if cur_ref_text != "":
                cur_bib_txt = cur_bib_txt + "\n" + cur_ref_text
        if cur_bib_txt != "":
            citation = cur_line_txt + "\n" + cur_bib_txt
            citations.append(citation)

    return citations

def extract_author_affiliation(soup):
    author_affiliation = nan
    try:
        file_soup = soup.select('teiHeader')[0].select('fileDesc')[0].select('sourceDesc')[0].select('biblstruct')[0]
        # pub_id = get_tei_element(file_soup, 'idno[2]')
        # txt_id = get_txt_id(pub_id)
        
        author_soup_lst = file_soup.select('author')
        for idx, author_soup in enumerate(author_soup_lst):
            surname = author_soup.surname.getText().strip()
            forename = author_soup.forename.getText().strip()
            orgname = author_soup.orgname.getText().strip()
            cur_author_affiliation = '{} {}({})'.format(surname, forename, orgname)
            if idx == 0:
                author_affiliation = cur_author_affiliation
            else:
                author_affiliation = author_affiliation + ', ' + cur_author_affiliation
    except Exception:
        pass
    return author_affiliation

def extract_publisher(soup):
    publisher = nan
    try:
        publisher_soup = soup.select('teiHeader')[0].select('fileDesc')[0].select('publicationStmt')[0]
        publisher = bscommon.get_element(publisher_soup, 'publisher')
    except Exception:
        pass
    return publisher

def extract_pubyr(soup):
    pub_yr = nan
    try:
        publisher_soup = soup.select('teiHeader')[0].select('fileDesc')[0].select('publicationStmt')[0]
        pub_yr = bscommon.get_element(publisher_soup, 'date')
    except Exception:
        pass
    return pub_yr

def get_ip(tei_folder):
    tei_fil_nam_lst = common.get_file_list(tei_folder, "tei")
    ip_lst = []
    for tei_fil_nam in tei_fil_nam_lst:
        with open(tei_fil_nam, 'r') as tei:
            soup = bs(tei, "xml")

            txt_id = common.get_unique_txt_id()

            abstract = extract_abstract(soup)

            file_cont = extract_file_contents(soup)

            citation = extract_citation(soup)

            author_affiliation = extract_author_affiliation(soup)
            publisher = extract_publisher(soup)
            pub_yr = extract_pubyr(soup)
            
            ip_lst.append((abstract, file_cont, citation, txt_id, author_affiliation, publisher, pub_yr))

    return ip_lst

if __name__ == "__main__":
    file_name = "/home/sigma/nanobubbles/script/cqpweb/Script/data/tei/tei_test.tei"
    with open(file_name, 'r') as tei:
        soup = bs(tei, "xml")

        # abstract = extract_abstract(soup)
        # print(abstract)

        file_contents = extract_file_contents(soup)
        print(file_contents)

        # cite_contents = extract_citation(soup)
        # print(cite_contents)

        # author_aff = extract_author_affiliation(soup)
        # print(author_aff)

        # publisher = extract_publisher(soup)
        # print(publisher)
