import pandas as pd

import common 

def get_ip(xslx_fil_nam):
    df = pd.read_excel(xslx_fil_nam, dtype=str)
    pub_id_lst = df['Publication ID'].tolist()
    txt_id_lst = [common.get_txt_id(pub_id) for pub_id in pub_id_lst]

    abstract_lst = df['Abstract'].tolist()

    author_affiliation_lst = df['Authors Affiliations'].tolist()
    publisher_lst = df['Publisher'].tolist()
    pub_yr_lst = df['PubYear'].tolist()
    
    cont_lst = [""] * len(pub_yr_lst)
    cite_lst = [""] * len(pub_yr_lst)

    ip_lst = list(zip(abstract_lst, cont_lst, cite_lst, txt_id_lst, author_affiliation_lst, publisher_lst, pub_yr_lst))

    return ip_lst
