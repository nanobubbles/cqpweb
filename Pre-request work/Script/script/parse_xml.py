# extract citation context from main content
from bs4 import BeautifulSoup as bs

import common
import bscommon

def delete_citation(cur_xml):
    temp = bscommon.delete_cur_xml_tag(cur_xml, "ce:cross-refs")
    cur_xml_wo_citations = bscommon.delete_cur_xml_tag(temp, "ce:cross-ref")

    return cur_xml_wo_citations

def extract_file_contents(soup):
    search_list = ["body", "ce:sections", "ce:section"]
    section_xml_list = bscommon.xml_get_children(soup, search_list)

    file_str = str()
    for cur_section_xml in section_xml_list:
        cur_section_str = str()
        cur_para_xml_list = bscommon.xml_get_children(cur_section_xml, ["ce:para"])
        for cur_para_xml in cur_para_xml_list:
            cur_para_str = str()
            inner_para = bscommon.get_inner_xml(cur_para_xml)
            lines = inner_para.split(". ")
            for line in lines:
                if not "<ce:cross-ref" in line:
                    line_soup = bs(line, "lxml")
                    line_str = line_soup.text.strip()

                    if line_str != "":
                        cur_para_str = cur_para_str + line_str + ". "

            if cur_para_str != "":
                cur_section_str = cur_section_str + cur_para_str + "\n"

        if cur_section_str != "":
            file_str = file_str + cur_section_str + "\n"

    return file_str
    # search_list = ["body", "ce:sections"]
    # cur_xml = bscommon.xml_get_child(soup, search_list)
    # cur_xml = delete_citation(cur_xml)
    # file_str = cur_xml.text
    # return file_str

def extract_abstract(soup):
    search_list = ["ce:abstract", "ce:simple-para"]
    cur_xml = bscommon.xml_get_child(soup, search_list)
    cur_xml = delete_citation(cur_xml)
    abs_str = cur_xml.text

    return abs_str

def find_cite_lines(para_xml):
    inner_para = bscommon.get_inner_xml(para_xml)
    lines = inner_para.split(". ")
    cite_lines = list()
    for line in lines:
        if "<ce:cross-ref" in line:
            cite_lines.append(line)

    return cite_lines

def find_bib_by_ref(bib_ref_list, id):
    for bib_ref in bib_ref_list:
        if id == bib_ref.get("id"):
            try:
                bib_txt = bib_ref.find("ce:source-text").text
            except Exception:
                bib_txt = bib_ref.find("ce:textref").text
            return bib_txt

def extract_citation(soup):
    search_list = ["body", "ce:sections", "ce:section"]
    cur_search_xml_list = bscommon.xml_get_children(soup, search_list)

    cite_lines = list()
    for cur_search_xml in cur_search_xml_list:
        cur_para_list = bscommon.xml_get_children(cur_search_xml, ["ce:para"])
        for cur_para in cur_para_list:
            cur_cite_lines = find_cite_lines(cur_para)
            cite_lines.extend(cur_cite_lines)

    citations = list()

    bib_ref_list = soup.find_all("ce:bib-reference")
    for cite_line in cite_lines:
        cite_line_soup = bs(cite_line, "lxml")
        cur_line_txt = cite_line_soup.text
        cross_ref_list = cite_line_soup.find_all("ce:cross-ref")
        cross_refs_list = cite_line_soup.find_all("ce:cross-refs")
        cross_ref_list.extend(cross_refs_list)
        cur_bib_txt = ""
        for cross_ref in cross_ref_list:
            id_str = cross_ref.get("refid")
            id_list = id_str.split(" ")
            cur_ref_text = ""
            for id in id_list:
                cur_id_text = find_bib_by_ref(bib_ref_list, id)
                if cur_id_text != None:
                    cur_ref_text = cur_ref_text + "\n" + cur_id_text
            if cur_ref_text != "":
                cur_bib_txt = cur_bib_txt + "\n" + cur_ref_text
        if cur_bib_txt != "":
            citation = cur_line_txt + "\n" + cur_bib_txt
            citations.append(citation)

    return citations

def extract_author_affiliation(soup):
    search_list = ["ce:author-group", "ce:author"]
    auth_xml_lst = bscommon.xml_get_children(soup, search_list)
    auth_names = str()
    for (idx, auth_xml) in enumerate(auth_xml_lst):
        auth_name = auth_xml.find("ce:given-name").text
        auth_surname = auth_xml.find("ce:surname").text
        if idx == 0:
            auth_names = auth_name + " " + auth_surname
        else:
            auth_names = auth_names + ", " + auth_name + " " + auth_surname

    search_list = ["ce:author-group", "ce:affiliation"]
    aff_xml_lst = bscommon.xml_get_children(soup, search_list)
    aff_names = str()
    for (idx, aff_xml) in enumerate(aff_xml_lst):
        aff = aff_xml.find("ce:textfn").text
        if idx == 0:
            aff_names = aff
        else:
            aff_names = aff_names + "; " + aff

    auth_text = auth_names + " : " + aff_names
    return auth_text

def extract_publisher(soup):
    search_list = ["coredata", "prism:publisher"]
    pub_xml = bscommon.xml_get_child(soup, search_list)
    return pub_xml.text

def extract_pubyr(soup):
    search_list = ["ce:date-accepted"]
    pubyr_xml = bscommon.xml_get_child(soup, search_list)
    pubyr = pubyr_xml.get("day") + "/" + pubyr_xml.get("month") + "/" + pubyr_xml.get("year") 
    return pubyr

def get_ip(xml_folder):
    xml_fil_nam_lst = common.get_file_list(xml_folder, "xml")
    ip_lst = []
    for xml_fil_nam in xml_fil_nam_lst:
        with open(xml_fil_nam, 'r') as xml:
            soup = bs(xml, "xml")

            txt_id = common.get_unique_txt_id()

            abstract = extract_abstract(soup)

            file_cont = extract_file_contents(soup)

            citation = extract_citation(soup)

            author_affiliation = extract_author_affiliation(soup)
            publisher = extract_publisher(soup)
            pub_yr = extract_pubyr(soup)

            ip_lst.append((abstract, file_cont, citation, txt_id, author_affiliation, publisher, pub_yr))

    return ip_lst

if __name__ == "__main__":
    # file_name = "/home/sigma/nanobubbles/script/cqpweb/Script/data/xml/elsapys0148296321007049.xml"
    file_name = "/home/sigma/nanobubbles/script/cqpweb/Script/data/xml/elsapys0378873321001088.xml"
    with open(file_name, 'r') as xml:
        soup = bs(xml, "xml")

        file_contents = extract_file_contents(soup)
        print(file_contents)

        # cite_contents = extract_citation(soup)
        # print(cite_contents)

        # author_aff = extract_author_affiliation(soup)
        # print(author_aff)

        # publisher = extract_publisher(soup)
        # print(publisher)
