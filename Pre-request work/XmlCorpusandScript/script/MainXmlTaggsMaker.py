import xml.etree.cElementTree as etree


root = etree.Element("root")
root.set("interesting", "totally")
# print(root.tag)

root.append(etree.Element("child1"))
root.append(etree.Element("child2"))
root.append(etree.Element("child3"))
root.text = "TEXT"
print(etree.tostring(root, xml_declaration=True, encoding="utf-8").decode())

# tree = etree.ElementTree(root)
# tree.docinfo()
# print(tree.docinfo.xml_version)