import xml.etree.cElementTree as etree
from bs4 import BeautifulSoup

def add_attr(node, attr_dict):
    for attr_dict_key in attr_dict.keys():
        node.set(attr_dict_key, attr_dict[attr_dict_key])

def add_seg(node, seg_txt):
    node_seg = etree.Element("s")
    node_seg.text = seg_txt
    node.append(node_seg)

def mk_vrt(meta_dic_lst, para_seg_txt_lst_lst):
    body = etree.Element("body")
        
    for (meta_dic, para_seg_txt_lst) in zip(meta_dic_lst, para_seg_txt_lst_lst):
        text = etree.Element("text")
        add_attr(text, meta_dic)

        para = etree.Element("p")
        for para_seg_txt in para_seg_txt_lst:
            add_seg(para, para_seg_txt)
        text.append(para)
        body.append(text)

    return body

def get_vrt_str(root_node):
    vrt_str = etree.tostring(root_node, encoding="utf-8").decode("utf-8")
    soup = BeautifulSoup(vrt_str, "lxml-xml")
    bs_vrt_str = soup.prettify()
    return bs_vrt_str

# Example
at_d = {
    "title" : "Manus .....",
    "id": "Bunchinger_1700_1",
    "collection": "historical",
    "ref": "none",
    
}
api_txt = mk_vrt([at_d], [["Werden .....", "bis .....", "und ....."]])
print(get_vrt_str(api_txt))