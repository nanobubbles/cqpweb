"""_summary_
in this module using beautifusoup liberary we traverse into tei, xml element by element to extract their data
"""
from cmath import nan
from hashlib import new

from bs4 import BeautifulSoup as bs

def get_element(soup, node_name):
    """_summary_
    in this function we pass parameters like soup, and node_name

    Args:
        soup (_type_): _description_
        node_name (_type_): _description_

    Returns:
        _type_: _description_
    """
    ele = nan
    """_summary_
    variable ele = nan
    """
    try:
        ele = getattr(soup, node_name).getText().strip()
        """_summary_
        function getattribute with parameter soup, node_name take text of each node without space

        """
    except Exception:
        pass
        """_summary_
        pass function without raising any exception and return text of each node
        """
    return ele

def delete_cur_xml_tag(cur_xml, delete_tag):
    """_summary_
     this function take cur_xml and delete_tag

    Args:
        cur_xml (_type_): _description_
        delete_tag (_type_): _description_

    Returns:
        _type_: find all the tag inside the body by .next properties
    """
    cur_xml_str = str(cur_xml) # by creating a string and then re-creating a new soup we are effectively doing a copy
    # this is needed coz in python everything is a reference and modifying it directly here will change the original soup
    """_summary_
    this means the argument cur_xml that we pass to the function is a referenced to the soup which exist in the memory any changes to cur_xml bring direct 
    changes to the soup, or in another way it is not cop independent copy of soup. the reason for recreating the soup is to work on the copy of the variable (soup)
    any chages bring on the copy of soup it does not affect the original soup.

    Returns:
        _type_: _description_
    """
    cur_xml_soup = bs(cur_xml_str, "lxml")
    """_summary_
    naming the new soup to cur_xml_soup

    Returns:
        _type_: _description_
    """
    # find_all changed to find
    delete_xmls = cur_xml_soup.find_all(delete_tag)
    """_summary_
    why we need to delete_tag?

    """
    for delete_xml in delete_xmls:
        """_summary_
        what is the need for this?
        """
        delete_xml.decompose()
        """_summary_
        ?

        """

    return cur_xml_soup.find("body").next

"""_summary_
how does these three functions work?
"""
def xml_get_child(root_xml, search_tag_list):
    cur_search_xml = root_xml
    for search in search_tag_list:
        cur_search_xml = cur_search_xml.find(search)
    return cur_search_xml

def xml_get_children(root_xml, search_tag_list):
    cur_search_xml = root_xml
    for (idx, search) in enumerate(search_tag_list):
        # if idx == len(search_tag_list) - 1:
        if idx == len(search_tag_list) - 1:
            cur_search_xml = cur_search_xml.find_all(search)
        # else:
        #     # find_all changed to find
        #     cur_search_xml = cur_search_xml.find(search)
    return cur_search_xml

def get_inner_xml(root_xml):
    return root_xml.encode_contents().decode()

    """_summary_
    https://python.hotexamples.com/examples/bs4/BeautifulSoup/encode_contents/python-beautifulsoup-encode_contents-method-examples.html
    """

# =====================================================================
# new

