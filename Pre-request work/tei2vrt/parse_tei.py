# extract citation context from main content
# this library give access to mathematical functions for complex numbers.
"""_summary_
for stefan project
"""
from cmath import nan
from tkinter.ttk import Separator
from xml.sax.handler import property_declaration_handler
# library use for web scraping purposes to pull the data out of html and xml files
# it creates a parse tree from page source code that can be used to extract data in hierarchical and more readable manner.
from bs4 import BeautifulSoup as bs
from soupsieve import select

import common
import bscommon
import os

from nltk import tokenize
# def delete_citation(cur_tei):
#     cur_tei_wo_citations = bscommon.delete_cur_xml_tag(cur_tei, "ref")
#     return cur_tei_wo_citations

def extract_abstract(soup):
    abs_str = bscommon.get_element(soup, 'abstract')
    return abs_str

# copied content from script 1

def extract_file_contents(soup):
    search_list = ["text", "body", "div"]
    section_tei_list = bscommon.xml_get_children(soup, search_list)

    file_str = str()
    for cur_section_tei in section_tei_list:
        cur_section_str = str()
        cur_para_tei_list = bscommon.xml_get_children(cur_section_tei, ["p"])
        for cur_para_tei in cur_para_tei_list:
            cur_para_str = str()
            inner_para = bscommon.get_inner_xml(cur_para_tei)
            lines = inner_para.split(". ")
            for line in lines:
                if not '<ref' or not 'bibr' in line:
                    line_soup = bs(line, "lxml")
                    line_str = line_soup.text.strip()

                    if line_str != "":
                        cur_para_str = cur_para_str + line_str + ". "

            if cur_para_str != "":
                cur_section_str = cur_section_str + cur_para_str + "\n"

        if cur_section_str != "":
            file_str = file_str + cur_section_str + "\n"

    return file_str


# original
def extract_author_affiliation(soup):
    # author_affiliation = []
    author_affiliation = ''

    try:
        file_soup = soup.select('teiHeader')[0].select('fileDesc')[0].select('sourceDesc')[0].select('biblStruct')[0].select('analytic')[0]
        author_soup_lst = file_soup.find_all('affiliation')
        #print(author_soup_lst)
    except Exception:
        pass
    #af_author_lst = []
    # print("==============================")
    for i in author_soup_lst:
        # print(i)
        # print("*************************")
        # author_affiliation.append((i.get_text().strip()).replace("\n", " "))
        author_affiliation = author_affiliation + ((i.get_text().strip()).replace("\n", " ")) + ","

    return author_affiliation

# # ***************************************************************************
def extract_authors(soup):

    authors = ''
    try:

        file_soup = soup.select('teiHeader')[0].select('fileDesc')[0].select('sourceDesc')[0].select('biblStruct')[0].select('analytic')[0]
        author_soup_lst = file_soup.find_all('persName')
    
    except Exception:
        pass

    for author_soup in author_soup_lst:

        surname = author_soup.surname.get_text().strip()
        firstname = author_soup.forename.get_text().strip()
        authors = authors + '{} {}'.format(surname,firstname) + ","

    return authors


# # *****************************************************************************
# # ===================================extract title=============================
def extract_title(soup):
    title = nan
    try:
        title_soup = soup.select('teiHeader')[0].select('fileDesc')[0].select('titleStmt')[0]
        title = bscommon.get_element(title_soup,'title')
        
    except Exception:
        pass

    return title

# # ===================================end of extract title======================

def extract_publisher(soup):
    publisher = nan
    try:
        publisher_soup = soup.select('teiHeader')[0].select('fileDesc')[0].select('publicationStmt')[0]
        publisher = bscommon.get_element(publisher_soup, 'publisher')
    except Exception:
        pass
    return publisher

def extract_pubyr(soup):
    pub_yr = nan
    try:
        publisher_soup = soup.select('teiHeader')[0].select('fileDesc')[0].select('publicationStmt')[0]
        pub_yr = bscommon.get_element(publisher_soup, 'date')
    except Exception:
        pass
    return pub_yr

def get_ip(tei_folder):
    print("++++++++")
    print("tei_folder: '"+str(tei_folder)+"'")
    #print("getcwd : '"+os.getcwd()+"'")
    print("++++++++")
    tei_fil_nam_lst = common.get_file_list(tei_folder, "tei")
    ip_lst = []
    for tei_fil_nam in tei_fil_nam_lst:
        new_path = os.path.join(tei_folder,tei_fil_nam)
        print("tei_file_nam : '"+tei_fil_nam+"'")
        #print("new_path : '"+new_path+"'")
        #with open(tei_fil_nam, 'r') as tei:
        with open(new_path, 'r') as tei:
            soup = bs(tei, "xml")

            txt_id = common.get_unique_txt_id()

            abstract = extract_abstract(soup)
            file_cont = extract_file_contents(soup)
            # citation = extract_citation(soup)
            title = extract_title(soup)
            authors = extract_authors(soup)
            author_affiliation = extract_author_affiliation(soup)
            publisher = extract_publisher(soup)
            pub_yr = extract_pubyr(soup)
            ip_lst.append((txt_id, abstract, file_cont, title, authors, author_affiliation, publisher, pub_yr))
    #print("++++++++ !!!!!!!!!!!!!!!!!!!!!!!!!!! +++++++++++++")
    return ip_lst


if __name__ == "__main__":
   
    file_name = '/home/sigma/Downloads/Nanobubbles_Extract_Tei_Content_Script/script/tei_input/d0na01005g.tei'
  
    with open(file_name, 'r') as tei:
        soup = bs(tei, "xml")

